<?php

return [
    // 'class' => 'yii\db\Connection',
    // 'dsn' => 'mysql:host=172.20.100.29:8080;dbname=cortex',
    // 'username' => 'root',
    // 'password' => '@cicc2018',
    // 'charset' => 'utf8',

    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=cortex',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
