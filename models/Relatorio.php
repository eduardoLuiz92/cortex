<?php

namespace app\models;


use app\models\Operacao;
use app\models\Recursooperacao;
use app\models\Orgao;
use app\models\Localidade;
use app\models\Indicador;
use yii\base\Model;
use DateTime;
use Yii;
use mPDF;


class Relatorio extends Model
{

  public static function getRecursos($op_id){
    $recursos = Recursooperacao::getRecursosOperacao($op_id);
    return $recursos;
  }

  public static function getLocalidades($op_id){
    $localidades = Localidade::getLocalidadesOperacao($op_id);
    $retorno = [];
    foreach ($localidades as $key => $value) {
      array_push($retorno, $value->attributes);
    }
    return $retorno;
  }

  public static function getOrgaosEnvolvidos($op_id){
    $recursos = Recursooperacao::getRecursosOperacao($op_id);
    $orgaos = [];
    foreach($recursos as $recurso){
      array_push($orgaos, Orgao::find()->where(['id'=>$recurso->orgao_id])->one()->descricao);
    }
    return $orgaos;
  }

  public static function getProdutividadeIndicadores($op_id){
    $produtividades = Produtividadeoperacao::find()->where(['operacao_id'=>$op_id])->all();
    $indicadores = [];
    $indicadores_aux = [];  // guarda os ids dos indicadores ja analisados
    foreach($produtividades as $produtividade){
      $indicador = Indicador::find()->where(['id'=>$produtividade->indicador_id])->one();

      if(!in_array($indicador->id,$indicadores_aux)){
        $indicador_na_op = [];
        $qtd = count(Produtividadeoperacao::find()->where(['operacao_id'=>$op_id, 'indicador_id'=>$produtividade->indicador_id])->all());
        $indicador_na_op["id"] = $indicador->id;
        $indicador_na_op["qtd"] = $qtd;
        $indicador_na_op["descricao"] = $indicador->descricao;
        array_push($indicadores, $indicador_na_op);
        array_push($indicadores_aux, $produtividade->indicador_id);
        
      }
    }

    return $indicadores;
  }

  public static function getTotalRecursos($op_id){
    
    $recursos = Recursooperacao::getTotalrecursosFormatadoParaImpressao($op_id);
    
    return $recursos;
  }

  public static function getProdutividadePorLocal($op_id = 53,$local_id = 175){
    $produtividades = [];

    $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("

            select 
              prod.descricao as 'Descrição',
              prod.qtd as 'Quantidade',
              prod.data as 'Data',
              prod.hora as 'Hora',
              loc.nome as 'Localidade',
              ind.descricao as 'Indicador',
              usu.username as 'Colaborador',
              org.sigla as 'Órgão'

            from 
              produtividade_operacao as prod
            inner join localidade as loc on loc.id = prod.localidade_id
            inner join indicador as ind on ind.id = prod.indicador_id
            inner join usuario as usu on usu.id = prod.usuario_id
            inner join orgao as org on org.id = usu.orgao_id

            where 
              prod.operacao_id = '$op_id'
            and
              prod.localidade_id = '$local_id'

            "
        );
        $dados = $sql->queryAll();

      if($dados){
        $produtividades[$dados[0]["Localidade"]] = $dados;
      }
    
    return $produtividades;
  }

  public static function getProdutividadesPorLocaisDaOperacao($op_id){
    $locais = Relatorio::getLocalidades($op_id);
    $prods = [];
    foreach ($locais as $local) {
      $informacoes = Relatorio::getProdutividadePorLocal($op_id, $local["id"]);
      if($informacoes){
        $aux = null;
        foreach ($informacoes as $key=>$value){
          $aux = $key;
          
        }
        $prods[$aux] = [];
        foreach ($informacoes as $key=>$value){
          $prods[$aux] += $value;

        }
      }
    }

    return $prods;

  }

  public static function relatorioGeralOperacao($op_id){
    
    $compilado = [];

    $op =  Operacao::find()->where(['id'=>$op_id])->one();
    $compilado["operacao"] = $op->attributes;
    $compilado["orgaos"] = Relatorio::getOrgaosEnvolvidos($op_id);
    $compilado["localidades"] = Relatorio::getLocalidades($op_id);
    $compilado["indicadores"] = Relatorio::getProdutividadeIndicadores($op_id);
    $compilado["recursos"] = Relatorio::getTotalRecursos($op_id);
    $compilado["produtividades"] = Relatorio::getProdutividadesPorLocaisDaOperacao($op_id);

    return $compilado;
  }

  public static function mes($mes){
    switch ($mes) {
      case '01':
        return 'janeiro';
        break;
      case '02':
        return 'fevereiro';
        break;
      case '03':
        return 'março';
        break;
      case '04':
        return 'abril';
        break;
      case '05':
        return 'maio';
        break;
      case '06':
        return 'junho';
        break;
      case '07':
        return 'julho';
        break;
      case '08':
        return 'agosto';
        break;
      case '09':
        return 'setembro';
        break;
      case '10':
        return 'outubro';
        break;
      case '11':
        return 'novembro';
        break;
      case '12':
        return 'dezembro';
        break;
      default:
        # code...
        break;
    }
  }

  // codigo de registro dos relatorios gerados 
  public static function codRegistroRelatorio($id_op){
    // data em milisegundos / timestamp;
    // id do usuario logado
    // secrete = cerebrumMaoAM
    // id da operação

    $usu_id = Yii::$app->user->identity->usuario_id;
    $secret = "cerebrumMaoAM";
    $dateTime = new DateTime();
    $timestamp = $dateTime->getTimestamp();
    $data = $timestamp;
    $id_operacao = $id_op;
    $registro = md5( base64_encode($data.$id_operacao.$usu_id.$secret) );
    return $registro;
  }

  public static function pdfRelatorio($dados, $id_op){

    $operacao = $dados["operacao"];
    $orgaos = $dados["orgaos"];
    $localidades = $dados["localidades"];
    $indicadores = $dados["indicadores"];
    $recursos = $dados["recursos"][0];
    $produtividades = $dados["produtividades"];

    $dateTime = new DateTime();
    $timestamp = $dateTime->getTimestamp();
    $data = $dateTime->setTimestamp($timestamp);
    $getTime = $dateTime->format( 'd-m-Y' ) . " | " . $data->format('U Y-m-d') . " | ". $timestamp;


    $mpdf = new \Mpdf\Mpdf();
    $stylesheet = file_get_contents('../web/css/css-cortex/relatorio.css');
    $mpdf->WriteHTML($stylesheet,1);

    $html = '<style type="text/css" media="print"> 

          @page{
            size: auto;
            margin: 15% 10% 15% 10% ;
            header: html_myHeader1;
            footer: html_myFooter1;
          }

          @media print {
            .produtividade_geral{
              /*page-break-before: always;*/
            }
            .assinatura, .detalhes_por_local{
              page-break-after: avoid;
            }
          }
          section{
            max-width: 900px;
            margin: 0 auto;
          }

          .assinatura{
            position: relative;
            bottom: 0;
            page-break-inside: avoid;
          }

          .secretario_adjunto, p {
            text-align: center;
            color: #333;
          }
          
          .data p{
            text-align: right;
            margin: 20px;
            margin-top: 40px;
            margin-right: 40px;
            margin-bottom: 40px;
            color: #333;
          }
        
          body{
            margin-top: 0px;
            margin-bottom: 20px;
            margin-left: 5em;
            margin-right: 5em;
          }
        
          .cabecalho{
            text-align: center;
          }
        
          .titulo{
            text-align: center;
            text-decoration: underline;
          }
        
          .topico{
            margin-left: 2em;
            margin-top: 10px;
            margin-bottom: 5px;
            text-align: justify;
          }
        
          .topico-content{
            margin-left: 3em;
          }
          .topico-content > ul > li{
            font-size: 16px;
            margin: 10px;
          }
          .topico-content > ul > li > ul >li{
            margin: 7px;
            margin-bottom: 15px;
            font-size: 14px;
          }

          td{
            color: #c3c3c3;
            font-size: 10px;  
          }

          .count_page{
            text-color: #333;
            font-size: 15px;  
          }

          .codigo_e_page {
            width= 100%;
          }

          </style>';
    
    $html .= "<body >";
    $html .= "<htmlpageheader name='myHeader1' style='display:none'>
          <div>
            <div class='cabecalho'>
              <img style=' margin-top: 0px; ' src='../web/images/logo do estado.png' alt=''>
            </div>
          </div>
      </htmlpageheader>";
    $html .= "<header class='cabecalho'>
          
          <div class='titulo'>
            <H2>". strtoupper($operacao['nome']) ."</H2>
          </div>
        </header>";
    
    $html .= "<section class='Dados gerais'>
          <div class='topico'>
            <H3>". mb_strtoupper("1) Dados Gerais", mb_internal_encoding()) ."</H3>
          </div>

          <div class='topico-content'>
            <ul>
              <li>
                <b>Datas: </b> ". $operacao['dt_referencia']
              ."</li> 
              <li>
                <b>Horário:</b> ". $operacao['hr_referencia'] ."
              </li>
              <li>
                <b>Órgãos Participantes: </b>
                  <ul>";
                    foreach ($orgaos as $key => $value):
                      $html .= "<li>". $value ."</li>";
                    endforeach; 
    $html .="</ul>
              </li>
            </ul>
          </div>
        </section>";

    $html .= "<section class='locais'>
          <div class='topico'>
            <H3>". mb_strtoupper("2) Locais: ", mb_internal_encoding())." ".count($localidades) ."</H3>
          </div>
          <div class='topico-content'>
            <ul>";
              foreach ($localidades as $key => $value):
                $html .= "<li> <b>" .$value['nome'] ."</b>";
                $html .= "<ul>" .$value['endereco'] ."</ul>";
                $html .= "</li>";
              endforeach; 
    $html.="</ul>
          </div>
        </section>";

    $html .= "<section class='recursos_gerais'>
          <div class='topico'>
            <H3>" .mb_strtoupper("3) recursos Gerais", mb_internal_encoding()) ."</H3>
          </div>
          <div class='topico-content'>
            <ul>";
              foreach ($recursos as $key => $value):
                if($value != 0):
                  $html .= "<li> <b>". $key ."</b>: ". $value ."</li>";
                endif;
              endforeach;
    $html.="</ul>
          </div>
        </section>";

    $html .= "<section class='produtividade_geral'>
          <div class='topico'>
            <H3>". mb_strtoupper("4) Produtividade Geral", mb_internal_encoding()) ."</H3>
          </div>
          <div class='topico-content'>
            <ul>";
              foreach ($indicadores as $key => $value):
                $html .= "<li> <b>". $value['descricao']."</b>: ".$value['qtd']."</li>";
              endforeach;
    $html.="</ul>
          </div>
        </section>";

    $html .= "<section class='detalhes_por_local'>
          <div class='topico'>
            <H3>". mb_strtoupper("5) Detalhes das Ações por Local", mb_internal_encoding()). "</H3>
          </div>
          <div class='topico-content'>
            <ul>";
              foreach ($produtividades as $key_local => $prods):
                $html .= "<li> <b>". mb_strtoupper($key_local, mb_internal_encoding()). "</b>:"; 
                $html .= "<ul>";
                    foreach ($prods as $key => $value):
                      $html .= "<li> <b>". $value["Órgão"]." - " .$value["Indicador"]."(".$value["Quantidade"].")". "</b>: ";
                      $html .= "<ul>
                          <li>
                            Descrição:".
                            $value["Descrição"]
                      ."</li>
                        </ul>
                      </li>";
                    endforeach;
                $html .= "</ul>
                </li>";
              endforeach;
    $html.="</ul>
          </div>
        </section>";
    $html .= "<section class='assinatura'>
        <div class='data'>
          <p>Manaus,". date('d'). " de ". Relatorio::mes(strval(date('m'))) ." de ". date("Y").
        "</div>
        <div class='secretario_adjunto'>
            <strong>HERMES SILVA DE MACEDO - CEL QOPM</strong>
            <p>Secretário Executivo Adjunto de Planejamento e Gestão Integrada de Segurança</p>
        </div>                
      </section>";
    $html .= '<htmlpagefooter name="myFooter1" style="display:none;">
                <table width="100%">
                  <tr>
                      <td width="33%">
                        <p>Avenida André Araújo, 1706 - Aleixo</p>
                        <p>Fone: (92) 3612-3127</p>
                        <p>Email: seagi@ssp.am.gov.br</p>
                        <p>Manaus/AM - CEP 69060-000</p>  
                      </td>
                      <td width="33%" style="text-align: left;">
                        <p>SECRETARIA DE ESTADO DE SEGURANÇA PÚBLICA</p>
                        <p>Secretaria Executiva Adjunta de</p>
                        <p>Planejamento e Gestão Integrada de</p>
                        <p>Segurança</p> 
                      </td>
                      <td width="33%" style="text-align: right; color: #333">
                        <img src="../web/images/logo-rodape.png" width= "40%">
                      </td>
                  </tr>
              </table>
              <table width="100%">
                <tr>
                  <td width="80%" style="text-align: left; color: #c3c3c3">
                    <p>'.Relatorio::codRegistroRelatorio($id_op).'</p>
                  </td>
                  <td width="20%" style="text-align: right; color: #333">
                    <p>{PAGENO}/{nbpg}</p>
                  </td>
                </tr>
              </table>
            </htmlpagefooter>';
    $html .= '</body>';
  
    $mpdf->WriteHTML($html);
    $mpdf->Output('RelatorioCerebrum-'.Relatorio::codRegistroRelatorio($id_op).'.pdf', 'I');
    exit;
  }
  
}