<?php

namespace app\models;
use app\models\Usuario;
use app\models\Logincerebrum;
use yii\db\ActiveRecord;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    public $nome_completo;
    public $cpf;
    public $email;
    public $telefone;
    public $password_reset_token;
    public $orgao_id;
    public $user_lvl;
    public $usuario_id;
    public $status_acesso;

    private static $users = [];

    public static function tableName()
    {
        // return 'usuario';
        return 'logincerebrum';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        // $user = Usuario::find()->where(['id'=>$id])->one();
        $user = Logincerebrum::find()->where(['id'=>$id])->one();

        if ($user) {
            return new static($user);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    public static function findByUsercpf($cpf){

        $users = Logincerebrum::find()->All();

        foreach ($users as $user) {
            if (strcasecmp($user['cpf'], $cpf) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
