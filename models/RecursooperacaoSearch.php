<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Recursooperacao;

/**
 * RecursooperacaoSearch represents the model behind the search form of `app\models\Recursooperacao`.
 */
class RecursooperacaoSearch extends Recursooperacao
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'operacao_id', 'orgao_id', 'efetivo', 'vtr_2_rodas', 'vtr_4_rodas', 'aeronave_asa_fixa', 'aeronave_asa_movel', 'ambulancia', 'onibus_microonibus', 'ht', 'van', 'embarcacao', 'helicoptero', 'plataforma', 'usuario_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recursooperacao::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'operacao_id' => $this->operacao_id,
            'orgao_id' => $this->orgao_id,
            'efetivo' => $this->efetivo,
            'vtr_2_rodas' => $this->vtr_2_rodas,
            'vtr_4_rodas' => $this->vtr_4_rodas,
            'aeronave_asa_fixa' => $this->aeronave_asa_fixa,
            'aeronave_asa_movel' => $this->aeronave_asa_movel,
            'ambulancia' => $this->ambulancia,
            'onibus_microonibus' => $this->onibus_microonibus,
            'ht' => $this->ht,
            'van' => $this->van,
            'embarcacao' => $this->embarcacao,
            'helicoptero' => $this->helicoptero,
            'plataforma' => $this->plataforma,
            'usuario_id' => $this->usuario_id,
        ]);

        return $dataProvider;
    }
}
