<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "produtividade_operacao".
 *
 * @property int $id
 * @property int $operacao_id
 * @property int $orgao_id
 * @property int $qtd
 * @property int $indicador_id
 * @property string $data
 * @property string $hora
 * @property int $usuario_id
 * @property int $localidade_id
 * @property string $descricao
 *
 * @property Anexo[] $anexos
 * @property Indicador $indicador
 * @property Localidade $localidade
 * @property Operacao $operacao
 * @property Orgao $orgao
 * @property Usuario $usuario
 */
class Produtividadeoperacao extends \yii\db\ActiveRecord
{

    public $nome_operacao;
    public $nome_orgao;
    public $nome_usuario;
    public $anexo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'produtividade_operacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['operacao_id', 'orgao_id', 'indicador_id', 'usuario_id','localidade_id','qtd'], 'required'],
            [['operacao_id', 'orgao_id', 'qtd', 'indicador_id', 'usuario_id', 'localidade_id'], 'integer'],
            [['descricao'], 'string'],
            [['anexo'],'file'],
            [['data', 'hora'], 'string', 'max' => 45],
            [['indicador_id'], 'exist', 'skipOnError' => true, 'targetClass' => Indicador::className(), 'targetAttribute' => ['indicador_id' => 'id']],
            [['localidade_id'], 'exist', 'skipOnError' => true, 'targetClass' => Localidade::className(), 'targetAttribute' => ['localidade_id' => 'id']],
            [['operacao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operacao::className(), 'targetAttribute' => ['operacao_id' => 'id']],
            [['orgao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orgao::className(), 'targetAttribute' => ['orgao_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operacao_id' => 'Operacao',
            'orgao_id' => 'Orgao',
            'qtd' => 'Quantidade',
            'indicador_id' => 'Indicador',
            'data' => 'Data',
            'hora' => 'Hora',
            'usuario_id' => 'Usuario',
            'localidade_id' => 'Localidade',
            'anexo'=>'Anexo',
            'descricao' => 'Descricao',
        ];
    }


    public static function getProdutividadeOperacaoEOrgao($operacao_id, $orgao_id){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            select  
            *
            from Produtividade_operacao
            where 
            operacao_id = {$operacao_id}
            and
            orgao_id = {$orgao_id}"
        );

        $dados = $sql->queryAll();
        $produtividades = [];
        foreach ($dados as $registro){
            $prod = new Produtividadeoperacao($registro);
            array_push($produtividades, $prod);
        }
        return $produtividades;
    }

    public static function getProdutividadeOperacao($operacao_id){
        $connection = Yii::$app->getDb();
        $indicadores = [];
        $produtividades = [];

        $produtividades_aux = Produtividadeoperacao::find()->where(['operacao_id'=>$operacao_id])->all();

        if($produtividades_aux){
            foreach ( $produtividades_aux as $produtividade) {
                $indicador_aux = Indicador::find()->where(['id' => $produtividade->indicador_id])->one();
                if( !in_array($indicador_aux->descricao,$indicadores) ){
                    array_push($indicadores, $indicador_aux->descricao);
                }
            }
    
            $query = " SELECT Orgao, ";
    
            foreach ($indicadores as $key => $value) {
                $query .= " SUM(CASE WHEN ind_Descricacao = '{$value}' THEN IndQTDE ELSE 0 END) AS '{$value}' ";
                if($key != (count($indicadores) - 1) ){
                    $query .= ", ";
                }
            }
    
            $query .= "FROM (
                SELECT org.sigla As Orgao, prod_op.qtd As IndQTDE, ind.descricao AS ind_Descricacao 
                FROM `produtividade_operacao` as prod_op
                LEFT JOIN `operacao` as op on prod_op.operacao_id = op.id
                LEFT JOIN `indicador` as ind on prod_op.indicador_id = ind.id
                LEFT JOIN `orgao` as org on prod_op.orgao_id = org.id
                LEFT JOIN `localidade` as loc on prod_op.localidade_id = loc.id
                where op.id = {$operacao_id}
                ORDER BY Orgao
            ) CONS
            GROUP BY Orgao";
            
            $sql = $connection->createCommand($query);
    
            $dados = $sql->queryAll();
            array_push($produtividades, $dados);
    
            $produtividades = [];
            foreach ($dados as $registro){
                array_push($produtividades, $registro);
            }
        }
                
        return $produtividades;
    }

    public static function getRelatorioProdutividadeOperacao($operacao_id){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            SELECT Orgao, SUM(CASE WHEN ind_Descricacao ='Apreensão' THEN IndQTDE ELSE 0 END) AS Apreensao
                ,SUM(CASE WHEN ind_Descricacao ='Prisão' THEN IndQTDE ELSE 0 END) AS Prisao
                ,SUM(CASE WHEN ind_Descricacao ='Auto de Notificação' THEN IndQTDE ELSE 0 END) AS Notificacao
                ,SUM(CASE WHEN ind_Descricacao ='Flagrante' THEN IndQTDE ELSE 0 END) AS Flagrante
                ,SUM(CASE WHEN ind_Descricacao ='Autuação' THEN IndQTDE ELSE 0 END) AS Autuacao
                ,SUM(CASE WHEN ind_Descricacao ='Multa' THEN IndQTDE ELSE 0 END) AS Multa
                ,SUM(CASE WHEN ind_Descricacao ='Recolhimento de CRLV' THEN IndQTDE ELSE 0 END) AS Recolhimento_de_CRLV
                ,SUM(CASE WHEN ind_Descricacao ='Intervenção' THEN IndQTDE ELSE 0 END) AS Intervencao
                ,SUM(CASE WHEN ind_Descricacao ='Condução' THEN IndQTDE ELSE 0 END) AS Conducao
                ,SUM(CASE WHEN ind_Descricacao ='Outros' THEN IndQTDE ELSE 0 END) AS Outros
                ,SUM(CASE WHEN ind_Descricacao ='Atendimento' THEN IndQTDE ELSE 0 END) AS Atendimento
                ,SUM(CASE WHEN ind_Descricacao ='Abordagem' THEN IndQTDE ELSE 0 END) AS Abordagem
                ,SUM(CASE WHEN ind_Descricacao ='interdição' THEN IndQTDE ELSE 0 END) AS interdicao
                ,SUM(CASE WHEN ind_Descricacao ='Alvará' THEN IndQTDE ELSE 0 END) AS Alvara
            FROM (
                SELECT org.sigla As Orgao, prod_op.qtd As IndQTDE, ind.descricao AS ind_Descricacao 
                FROM `produtividade_operacao` as prod_op
                LEFT JOIN `operacao` as op on prod_op.operacao_id = op.id
                LEFT JOIN `indicador` as ind on prod_op.indicador_id = ind.id
                LEFT JOIN `orgao` as org on prod_op.orgao_id = org.id
                LEFT JOIN `localidade` as loc on prod_op.localidade_id = loc.id
                where op.id = {$operacao_id}
                ORDER BY Orgao
            ) CONS
            GROUP BY Orgao"
        );

        $dados = $sql->queryAll();
        $produtividades = [];
        foreach ($dados as $registro){
            // $prod = new Produtividadeoperacao($registro);
            array_push($produtividades, $registro);
        }
        return $produtividades;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnexos()
    {
        return $this->hasMany(Anexo::className(), ['produtividade_operacao_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicador()
    {
        return $this->hasOne(Indicador::className(), ['id' => 'indicador_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidade()
    {
        return $this->hasOne(Localidade::className(), ['id' => 'localidade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacao()
    {
        return $this->hasOne(Operacao::className(), ['id' => 'operacao_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgao()
    {
        return $this->hasOne(Orgao::className(), ['id' => 'orgao_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
