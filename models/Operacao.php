<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operacao".
 *
 * @property int $id
 * @property string $nome
 * @property string $municipio_desc
 * @property int $municipio_id
 * @property int $orgao_responsavel_id
 * @property string $dt_referencia
 * @property string $hr_referencia
 * @property int $usuario_id
 *
 * @property Observacao[] $observacaos
 * @property Orgao $orgaoResponsavel
 * @property Usuario $usuario
 * @property ProdutividadeOperacao[] $produtividadeOperacaos
 * @property RecursoOperacao[] $recursoOperacaos
 */
class Operacao extends \yii\db\ActiveRecord
{

    public $nome_aux;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'operacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['municipio_id', 'orgao_responsavel_id'], 'integer'],
            [['dt_referencia', 'hr_referencia','nome','orgao_responsavel_id', 'usuario_id'], 'required'],
            [['nome'], 'string', 'max' => 100],
            [['municipio_desc', 'dt_referencia', 'hr_referencia'], 'string', 'max' => 45],
            [['orgao_responsavel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orgao::className(), 'targetAttribute' => ['orgao_responsavel_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome da Operação',
            'municipio_desc' => 'Município Descrição',
            'municipio_id' => 'Município',
            'orgao_responsavel_id' => 'Órgão Organizador',
            'dt_referencia' => 'Data Referência',
            'hr_referencia' => 'Hora Referência',
            'usuario_id' => 'Usuário',
            'nome_aux' => 'Usuário Responsável'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservacaos()
    {
        return $this->hasMany(Observacao::className(), ['operacao_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgaoResponsavel()
    {
        return $this->hasOne(Orgao::className(), ['id' => 'orgao_responsavel_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdutividadeOperacaos()
    {
        return $this->hasMany(ProdutividadeOperacao::className(), ['operacao_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursoOperacaos()
    {
        return $this->hasMany(RecursoOperacao::className(), ['operacao_id' => 'id']);
    }
}
