<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recurso_operacao".
 *
 * @property int $id
 * @property int $operacao_id
 * @property int $orgao_id
 * @property int $efetivo
 * @property int $vtr_2_rodas
 * @property int $vtr_4_rodas
 * @property int $aeronave_asa_fixa
 * @property int $aeronave_asa_movel
 * @property int $ambulancia
 * @property int $onibus_microonibus
 * @property int $ht
 * @property int $van
 * @property int $embarcacao
 * @property int $helicoptero
 * @property int $plataforma
 * @property int $usuario_id
 *
 * @property Operacao $operacao
 * @property Orgao $orgao
 * @property Usuario $usuario
 */
class Recursooperacao extends \yii\db\ActiveRecord
{

    public $nome_operacao;
    public $nome_orgao_responsavel;
    public $nome_usuario_responsavel;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recurso_operacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['operacao_id', 'orgao_id', 'usuario_id'], 'required'],
            [['operacao_id', 'orgao_id', 'efetivo', 'vtr_2_rodas', 'vtr_4_rodas', 'aeronave_asa_fixa', 'aeronave_asa_movel', 'ambulancia', 'onibus_microonibus', 'ht', 'van', 'embarcacao', 'helicoptero', 'plataforma', 'usuario_id'], 'integer'],
            [['operacao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operacao::className(), 'targetAttribute' => ['operacao_id' => 'id']],
            [['orgao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orgao::className(), 'targetAttribute' => ['orgao_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operacao_id' => 'Operacao ID',
            'orgao_id' => 'Orgao ID',
            'efetivo' => 'Efetivo',
            'vtr_2_rodas' => 'VTR 2 Rodas',
            'vtr_4_rodas' => 'VTR 4 Rodas',
            'aeronave_asa_fixa' => 'Aeronave Asa Fixa',
            'aeronave_asa_movel' => 'Aeronave Asa Móvel',
            'ambulancia' => 'Ambulância',
            'onibus_microonibus' => 'Ônibus/Microonibus',
            'ht' => 'HT',
            'van' => 'Van',
            'embarcacao' => 'Embarcação',
            'helicoptero' => 'Helicoptero',
            'plataforma' => 'Plataforma',
            'usuario_id' => 'Usuario ID',
            'nome_operacao' => 'Operação',
            'nome_orgao_responsavel' => 'Orgão responsavél pelo recurso',
            'nome_usuario_responsavel' => 'Responsável pelos Recursos'
        ];
    }

    public static function getAllRecursosOperacao($operacao_id){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            select  
            *
            from recurso_operacao
            where 
            operacao_id = {$operacao_id}"
        );

        $dados = $sql->queryAll();
        $produtividades = [];
        foreach ($dados as $registro){
            $prod = new Recursooperacao($registro);
            array_push($produtividades, $prod);
        }
        return $produtividades;
    }

    public static function getTotalrecursos($opid){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand(
            "select 
                sum(efetivo) as efetivo,
                sum(vtr_2_rodas) as vtr_2_rodas,
                sum(vtr_4_rodas) as vtr_4_rodas,
                sum(aeronave_asa_fixa) as aeronave_asa_fixa,
                sum(aeronave_asa_movel) as aeronave_asa_movel,
                sum(ambulancia) as ambulancia,
                sum(onibus_microonibus) as onibus_microonibus,
                sum(ht) as ht,
                sum(van) as van,
                sum(embarcacao) as embarcacao,
                sum(helicoptero) as helicoptero,
                sum(plataforma) as plataforma
            from recurso_operacao
            where operacao_id = {$opid}"
        );
        $dados = $sql->queryAll();
        $recursos = [];
        foreach ($dados as $registro){
            $recurso = new Recursooperacao($registro);
            array_push($recursos, $recurso);
        }
        return $recursos;
    }

    public static function getTotalrecursosFormatadoParaImpressao($opid){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand(
            "select 
                sum(efetivo) as 'Efetivo',
                sum(vtr_2_rodas) as 'VTR 2 rodas',
                sum(vtr_4_rodas) as 'VTR 4 rodas',
                sum(aeronave_asa_fixa) as 'Aeronave asa fixa',
                sum(aeronave_asa_movel) as 'Aeronave asa movel',
                sum(ambulancia) as 'Ambulância',
                sum(onibus_microonibus) as 'Ônibus/Microonibus',
                sum(ht) as 'HT',
                sum(van) as 'Van',
                sum(embarcacao) as 'Embarcação',
                sum(helicoptero) as 'Helicoptero',
                sum(plataforma) as 'Plataforma'
            from recurso_operacao
            where operacao_id = {$opid}"
        );
        $dados = $sql->queryAll();

        return $dados;
    }

    public static function getRecursosOperacao($operacao_id){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
        select 
            id,
            operacao_id,
            orgao_id,
            sum(efetivo) as efetivo,
            sum(vtr_2_rodas) as vtr_2_rodas,
            sum(vtr_4_rodas) as vtr_4_rodas,
            sum(aeronave_asa_fixa) as aeronave_asa_fixa,
            sum(aeronave_asa_movel) as aeronave_asa_movel,
            sum(ambulancia) as ambulancia,
            sum(onibus_microonibus) as onibus_microonibus,
            sum(ht) as ht,
            sum(van) as van,
            sum(embarcacao) as embarcacao,
            sum(helicoptero) as helicoptero,
            sum(plataforma) as plataforma
        from recurso_operacao
        where operacao_id = {$operacao_id}
        group by orgao_id
        order by orgao_id"
        );

        $dados = $sql->queryAll();
        $recursos = [];
        foreach ($dados as $registro){
            $recurso = new Recursooperacao($registro);
            array_push($recursos, $recurso);
        }
        return $recursos;
    }

    public static function getRecursosPorOrgaoEOperacao($operacao_id,$orgao_id){
        
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            select  
            *
            from Recurso_operacao
            where 
            operacao_id = {$operacao_id}
            and
            orgao_id = {$orgao_id}"
        );

        $dados = $sql->queryAll();
        $recursos = [];
        foreach ($dados as $registro){
            $recurso = new Recursooperacao($registro);
            array_push($recursos, $recurso);
        }
        return $recursos;
    }

    public static function getTotalRecursosOrgao($opid,$orgao_id){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            select 
                operacao_id,
                orgao_id,
                sum(efetivo) as efetivo,
                sum(vtr_2_rodas) as vtr_2_rodas,
                sum(vtr_4_rodas) as vtr_4_rodas,
                sum(aeronave_asa_fixa) as aeronave_asa_fixa,
                sum(aeronave_asa_movel) as aeronave_asa_movel,
                sum(ambulancia) as ambulancia,
                sum(onibus_microonibus) as onibus_microonibus,
                sum(ht) as ht,
                sum(van) as van,
                sum(embarcacao) as embarcacao,
                sum(helicoptero) as helicoptero,
                sum(plataforma) as plataforma
            from recurso_operacao
            where 
                operacao_id = {$opid}
            and
                recurso_operacao.orgao_id = {$orgao_id}
            group by orgao_id
            order by orgao_id"
        );

        $dados = $sql->queryAll();
        $produtividades = [];
        foreach ($dados as $registro){
            $prod = new Recursooperacao($registro);
            array_push($produtividades, $prod);
        }
        return $produtividades;
        // return $dados;

        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacao()
    {
        return $this->hasOne(Operacao::className(), ['id' => 'operacao_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgao()
    {
        return $this->hasOne(Orgao::className(), ['id' => 'orgao_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
