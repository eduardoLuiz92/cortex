<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recurso".
 *
 * @property int $id
 * @property string $descricao
 *
 * @property OrgaoHasRecurso[] $orgaoHasRecursos
 * @property Orgao[] $orgaos
 */
class Recurso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recurso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descricao'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descricao' => 'Descricao',
        ];
    }

    public static function getTotalRecursosOrgao($opid,$orgao_id){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            select 
                operacao_id,
                orgao_id,
                sum(efetivo) as efetivo,
                sum(vtr_2_rodas) as vtr_2_rodas,
                sum(vtr_4_rodas) as vtr_4_rodas,
                sum(aeronave_asa_fixa) as aeronave_asa_fixa,
                sum(aeronave_asa_movel) as aeronave_asa_movel,
                sum(ambulancia) as ambulancia,
                sum(onibus_microonibus) as onibus_microonibus,
                sum(ht) as ht,
                sum(van) as van,
                sum(embarcacao) as embarcacao,
                sum(helicoptero) as helicoptero,
                sum(plataforma) as plataforma
            from recurso_operacao
            where 
                operacao_id = {$opid}
            and
                recurso_operacao.orgao_id = {$orgao_id}
            group by orgao_id
            order by orgao_id"
        );

        $dados = $sql->queryAll();
        // $produtividades = $dados;
        // foreach ($dados as $registro){
        //     $prod = new Produtividadeoperacao($registro);
        //     array_push($produtividades, $prod);
        // }
        // return $produtividades;
        return $dados;

        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgaoHasRecursos()
    {
        return $this->hasMany(OrgaoHasRecurso::className(), ['recurso_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgaos()
    {
        return $this->hasMany(Orgao::className(), ['id' => 'orgao_id'])->viaTable('orgao_has_recurso', ['recurso_id' => 'id']);
    }
}
