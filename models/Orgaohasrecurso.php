<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orgao_has_recurso".
 *
 * @property int $orgao_id
 * @property int $recurso_id
 *
 * @property Orgao $orgao
 * @property Recurso $recurso
 */
class Orgaohasrecurso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orgao_has_recurso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orgao_id', 'recurso_id'], 'required'],
            [['orgao_id', 'recurso_id'], 'integer'],
            [['orgao_id', 'recurso_id'], 'unique', 'targetAttribute' => ['orgao_id', 'recurso_id']],
            [['orgao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orgao::className(), 'targetAttribute' => ['orgao_id' => 'id']],
            [['recurso_id'], 'exist', 'skipOnError' => true, 'targetClass' => Recurso::className(), 'targetAttribute' => ['recurso_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'orgao_id' => 'Orgao ID',
            'recurso_id' => 'Recurso ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgao()
    {
        return $this->hasOne(Orgao::className(), ['id' => 'orgao_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecurso()
    {
        return $this->hasOne(Recurso::className(), ['id' => 'recurso_id']);
    }
}
