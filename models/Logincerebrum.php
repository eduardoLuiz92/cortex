<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "login".
 *
 * @property int $id
 * @property int $usuario_id
 * @property string $username
 * @property string $cpf
 * @property string $password
 * @property string $user_lvl
 * @property string $status_acesso
 *
 * @property Usuario $usuario
 */
class Logincerebrum extends \yii\db\ActiveRecord
{

    CONST STATUS_ATIVO      = 'ATIVO';
    CONST STATUS_INATIVO    = 'INATIVO';

    CONST USUARIO_ADM       = 'ADMIN';
    CONST USUARIO_ADM_ORGAO = 'ADM_ORGAO';
    CONST USUARIO_COMUM     = 'COMUM';

    CONST STATUS_ACESSO = [
        Logincerebrum::STATUS_ATIVO      => 'ATIVO',
        Logincerebrum::STATUS_INATIVO    => 'INATIVO'
    ];

    CONST TIPOS_USUARIOS = [
        Logincerebrum::USUARIO_ADM          => 'Administrador',
        Logincerebrum::USUARIO_ADM_ORGAO    => 'Adm de Orgão',
        Logincerebrum::USUARIO_COMUM        => 'Comum'  
    ];

    CONST TIPOS_USUARIOS_POR_ORGAO = [
        Logincerebrum::USUARIO_ADM_ORGAO    => 'Adm de Orgão',
        Logincerebrum::USUARIO_COMUM        => 'Comum'  
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'login';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'cpf', 'user_lvl', 'status_acesso'], 'required'],
            [['usuario_id'], 'integer'],
            [['username', 'cpf', 'password', 'user_lvl', 'status_acesso'], 'string', 'max' => 45],
            [['cpf'], 'unique'],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'username' => 'Nome de Usuário',
            'cpf' => 'CPF',
            'password' => 'Senha',
            'user_lvl' => 'Tipo de Usuário',
            'status_acesso' => 'Acesso',
        ];
    }

    public static function mesmoOrgao($usuario_id){
        $mesmo_orgao = false;
        $usuario = Usuario::find()->where(['id'=>$usuario_id])->one();
        if(!Yii::$app->user->isGuest){
            $usuario_logado = Usuario::find()->where(['id'=>Yii::$app->user->identity->usuario_id])->one();
            if($usuario_logado){
                if($usuario_logado->orgao_id == $usuario->orgao_id){
                    $mesmo_orgao = true;
                }
            }
        }
        return $mesmo_orgao;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    public function usuarioSemAcesso(){
        $connection = Yii::$app->getDb();
        $usuarios = [];
        $sql = $connection->createCommand("
            select * from usuario as u
            where not exists (
                select * from login as l
                where u.cpf = l.cpf
            )
            ");
        $dados = $sql->queryAll();
        foreach($dados as $key => $value){
            $aux = new Usuario();
            $aux->attributes = $value;
            $aux->id = $value['id'];
            array_push($usuarios, $aux);
        }
        return $usuarios;
    }

    public static function verificaSeUsuarioTemAcesso($id){
        $login = Logincerebrum::find()->where(['usuario_id'=>$id])->one();
        $acesso = '';
        if($login){
            $acesso = $login->status_acesso;
        }
        return $acesso;
    }
}
