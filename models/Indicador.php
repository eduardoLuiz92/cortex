<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "indicador".
 *
 * @property int $id
 * @property string $descricao
 *
 * @property ProdutividadeOperacao[] $produtividadeOperacaos
 */
class Indicador extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'indicador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descricao'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descricao' => 'Descrição',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdutividadeOperacaos()
    {
        return $this->hasMany(ProdutividadeOperacao::className(), ['indicador_id' => 'id']);
    }
}
