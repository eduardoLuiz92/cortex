<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "localidade".
 *
 * @property int $id
 * @property string $endereco
 * @property string $latitude
 * @property string $longitude
 * @property int $operacao_id
 * @property string $nome
 *
 * @property Operacao $operacao
 */
class Localidade extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'localidade';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['operacao_id','endereco','nome'], 'required'],
            [['operacao_id'], 'integer'],
            [['endereco'], 'string', 'max' => 200],
            [['latitude', 'longitude'], 'string', 'max' => 45],
            [['nome'], 'string', 'max' => 100],
            [['operacao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operacao::className(), 'targetAttribute' => ['operacao_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'endereco' => 'Endereco',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'operacao_id' => 'Operacao ID',
            'nome' => 'Nome',
        ];
    }

    public static function getLocalidadesOperacao($operacao_id){
        $connection = Yii::$app->getDb();
        $sql = $connection->createCommand("
            select  
            *
            from localidade
            where 
            operacao_id = {$operacao_id}"
        );

        $dados = $sql->queryAll();
        $localidades = [];
        foreach ($dados as $registro){
            $prod = new Localidade($registro);
            array_push($localidades, $prod);
        }
        return $localidades;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacao()
    {
        return $this->hasOne(Operacao::className(), ['id' => 'operacao_id']);
    }
}
