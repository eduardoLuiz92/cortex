<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orgao".
 *
 * @property int $id
 * @property string $descricao
 * @property string $sigla
 *
 * @property Operacao[] $operacaos
 * @property OrgaoHasRecurso[] $orgaoHasRecursos
 * @property Recurso[] $recursos
 * @property ProdutividadeOperacao[] $produtividadeOperacaos
 * @property RecursoOperacao[] $recursoOperacaos
 * @property Usuario[] $usuarios
 */
class Orgao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orgao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descricao', 'sigla'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descricao' => 'Descrição',
            'sigla' => 'Sigla',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacaos()
    {
        return $this->hasMany(Operacao::className(), ['orgao_responsavel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgaoHasRecursos()
    {
        return $this->hasMany(OrgaoHasRecurso::className(), ['orgao_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursos()
    {
        return $this->hasMany(Recurso::className(), ['id' => 'recurso_id'])->viaTable('orgao_has_recurso', ['orgao_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdutividadeOperacaos()
    {
        return $this->hasMany(ProdutividadeOperacao::className(), ['orgao_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecursoOperacaos()
    {
        return $this->hasMany(RecursoOperacao::className(), ['orgao_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['orgao_id' => 'id']);
    }
}
