<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $username
 * @property string $nome_completo
 * @property string $cpf
 * @property string $email
 * @property string $telefone
 * @property string $authKey
 * @property string $password
 * @property string $password_reset_token
 * @property string $user_level
 * @property int $orgao_id
 * @property string $accessToken
 *
 * @property Log[] $logs
 * @property Observacao[] $observacaos
 * @property Operacao[] $operacaos
 * @property Orgao $orgao
 * @property UserLvl $userLvlDi
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome_completo','password','cpf','orgao_id'], 'required', 'message'=>'Campo não pode ser em branco'],
            [['orgao_id'], 'integer'],
            [['cpf'],'unique'],
            [['accessToken'], 'string'],
            [['username', 'cpf', 'email', 'telefone', 'authKey', 'password', 'password_reset_token', 'user_level'], 'string', 'max' => 45],
            [['nome_completo'], 'string', 'max' => 200],
            [['orgao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orgao::className(), 'targetAttribute' => ['orgao_id' => 'id']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Usuário (Como devo chamá-lo(a))',
            'nome_completo' => 'Nome Completo',
            'cpf' => 'CPF',
            'email' => 'E-mail',
            'telefone' => 'Telefone',
            'authKey' => 'Auth Key',
            'password' => 'Senha',
            'password_reset_token' => 'Password Reset Token',
            'user_level' => 'User Level',
            'orgao_id' => 'Órgão',
            'accessToken' => 'Access Token'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObservacaos()
    {
        return $this->hasMany(Observacao::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperacaos()
    {
        return $this->hasMany(Operacao::className(), ['usuario_responsavel_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgao()
    {
        return $this->hasOne(Orgao::className(), ['id' => 'orgao_id']);
    }
}
