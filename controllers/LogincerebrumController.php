<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Logincerebrum;
use app\models\LogincerebrumSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Usuario;

/**
 * LogincerebrumController implements the CRUD actions for Logincerebrum model.
 */
class LogincerebrumController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout','index','view','create','update','delete','mudarstatus'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete','mudarstatus'],
                        'matchCallback' => function($rule,$action){
                            if(!Yii::$app->user->isGuest){
                                // echo "<pre>"; print_r(Yii::$app->user->identity->user_level);die;
                                return true;
                                // if(Yii::$app->user->identity->user_lvl_di == 1){
                                //     return true;
                                // }else{
                                //     return false;
                                // }
                            }
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Logincerebrum models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogincerebrumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Logincerebrum model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Logincerebrum model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id="")
    {
        $model = new Logincerebrum();
        $usuarios_sem_acesso = $model->usuarioSemAcesso();
        $usuario = null;
        
        if($id != ""){
            $usuario = Usuario::findOne($id);
            $model->attributes = $usuario->attributes;
            $model->usuario_id = $usuario->id;
            
        }
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $model->usuario_id = Yii::$app->request->post('usuario_id');
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'usuarios' => $usuarios_sem_acesso
        ]);
    }

    public function actionMudarstatus($id){
        
        $permissao_para_ativar = false;
        if(Yii::$app->user->identity->user_lvl != Logincerebrum::USUARIO_ADM){
            if(Logincerebrum::mesmoOrgao($id)){
                $permissao_para_ativar = true;
            }
        }else{
            $permissao_para_ativar = true;
        }

        if($permissao_para_ativar == true){
            $usuario = Logincerebrum::find()->where(['usuario_id'=>$id])->one();
            if($usuario->status_acesso == Logincerebrum::STATUS_INATIVO){
                $usuario->status_acesso = Logincerebrum::STATUS_ATIVO;
            }else{
                $usuario->status_acesso = Logincerebrum::STATUS_INATIVO;
            }
    
            if($usuario->save()){
                return $this->redirect(['usuario/index']);
            }
        }
        return $this->redirect(['usuario/index']);
    }

    /**
     * Updates an existing Logincerebrum model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Logincerebrum model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (\Throwable $th) {
            //throw $th;
        }
        return $this->redirect(['index','status'=>'400','message'=>'não foi possivel excluir']);
    }

    /**
     * Finds the Logincerebrum model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logincerebrum the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Logincerebrum::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
