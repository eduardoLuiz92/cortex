<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Orgao;
use app\models\Logincerebrum;
use app\models\OrgaoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrgaoController implements the CRUD actions for Orgao model.
 */
class OrgaoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout','create','index','delete','update','getorgaos'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create','view','index','delete','update'],
                        'matchCallback' => function($rule,$action){
                                if(!Yii::$app->user->isGuest){
                                    // echo "<pre>"; print_r(Yii::$app->user->identity->user_level);die;
                                    if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM){
                                        return true;
                                    }else{
                                        return false;
                                    }
                                }
                            },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Orgao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrgaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetorgaos(){
        $orgaos = [];
        foreach(Orgao::find()->all() as $orgao){
            array_push($orgaos,['id'=>$orgao->id,'descricao'=>$orgao->sigla]);
        }
        return json_encode($orgaos);
    }

    /**
     * Displays a single Orgao model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Orgao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orgao();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Orgao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Orgao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (\Throwable $th) {
            //throw $th;
        }
        
        return $this->redirect(['index','status'=>'400','message'=>'não foi possivel excluir']);
    }

    /**
     * Finds the Orgao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orgao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orgao::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
