<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use yii\filters\AccessControl;
use app\models\UsuarioSearch;
use app\models\Logincerebrum;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout','create','index','delete','update','solicitacaodeacesso'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login','getusuario','solicitacaodeacesso'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create','view','index','delete','update'],
                        'matchCallback' => function($rule,$action){
                                if(!Yii::$app->user->isGuest){
                                    if(Yii::$app->user->identity->user_lvl != Logincerebrum::USUARIO_COMUM){
                                        return true;
                                    }else{
                                        return false;
                                    }
                                }
                            },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $login = Logincerebrum::find()->where(['usuario_id'=>$id])->one();
        
        return $this->render('view-novo', [
            'model' => $this->findModel($id),
            'login' => $login
        ]);
    }
    
    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSolicitacaodeacesso()
    {
        $model = new Usuario();
        $login = new Logincerebrum();
        $mensagem = '';
        

        if(Yii::$app->request->post()){
            
            $model->load(Yii::$app->request->post());
            $login->load(Yii::$app->request->post());

            // monta objeto login 
            $login->username = $model->username;
            $login->status_acesso = Logincerebrum::STATUS_INATIVO;
            $login->cpf = $model->cpf;
            $login->password = $model->password;
            $login->user_lvl = Logincerebrum::USUARIO_COMUM;

            $usuario_existente = Usuario::find()->where(['cpf'=>$model->cpf])->one();
            if($usuario_existente){
                
                $mensagem = 'errocpf';
                return $this->render('create', [
                    'model' => $model,
                    'mensagem' => $mensagem
                ]);
            }
            
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->save()) {
                    $login->usuario_id = $model->id;
    
                    if($login->save()){
                        $transaction->commit();
                        $mensagem = 'sucesso';
                        return $this->redirect(['site/index', 'mensagem' => $mensagem]);
                    }
                }
            } catch (\Throwable $th) {
                $transaction->rollBack();
            }
        }

        return $this->render('create', [
            'model' => $model,
            'mensagem' => $mensagem
        ]);
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuario();
        $login = new Logincerebrum();
        $mensagem = '';
        if(Yii::$app->request->post()){
            
            $model->load(Yii::$app->request->post());
            $login->load(Yii::$app->request->post());
            // monta objeto login 
            $login->username = $model->username;
            $login->status_acesso = Logincerebrum::STATUS_ATIVO;
            $login->cpf = $model->cpf;
            $login->password = $model->password;

            $transaction = Yii::$app->db->beginTransaction();

            try {
                if ($model->save()) {
                    $login->usuario_id = $model->id;
    
                    if($login->save()){
                        $transaction->commit();
                        $mensagem = 'sucesso';
                        return $this->redirect(['view', 'id' => $model->id, 'mensagem'=>$mensagem]);
                    }
                }
            } catch (\Throwable $th) {
                $transaction->rollBack();
                $mensagem = 'erro';
            }
        }

        return $this->render('create', [
            'model' => $model,
            'mensagem' => $mensagem
        ]);
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $login = Logincerebrum::find()->where(['usuario_id'=>$id])->one();
        $mensagem = "sucesso";

        if(Yii::$app->request->post()){

            $model->load(Yii::$app->request->post());
            $login->load(Yii::$app->request->post());

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if($model->save()){
                    $login->username = $model->username;
                    $login->cpf = $model->cpf;
                    $login->password = $model->password;

                    if($login->save()){
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id, 'mensagem'=>$mensagem]);
                    }else{
                        $mensagem = "erro";
                    }
                }else{
                    $mensagem = "erro";
                }
            } catch (\Throwable $th) {
                $transaction->rollBack();
            }   
        }

        return $this->render('update', [
            'model' => $model,
            'login' => $login
        ]);
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (\Throwable $th) {
            //throw $th;
        }
        return $this->redirect(['index','status'=>'400','message'=>'não foi possivel excluir']);
        
    }

    public function actionGetusuario(){
        if(Yii::$app->request->get()){

            $usuario = new Usuario();
            $data = null;
            
            try {
                if(Yii::$app->request->get('id')){
                    $id = Yii::$app->request->get('id');
                    $usuario->id = $id;
                    $usuario = Usuario::findOne($usuario->id);
                    if($usuario){
                        $data = [
                            'cpf'=>$usuario->cpf,
                            'username'=>$usuario->username
                        ];
                    }
                    return json_encode(['message'=>'sucesso','data'=>$data,'status'=>200]);
                }else{
                    return json_encode(['message'=>'informação incorreta','data'=>'','status'=>400]);
                }
            } catch (\Throwable $th) {
                return json_encode(['message'=>'Usuario não encontrado','data'=>'','status'=>400]);
            }
            
        }
        return json_encode(['message'=>'Usuario não encontrado fora','data'=>'','status'=>400]);
    }

    // Caso haja novamente uma perca de dados do servidor, é só executar esse método para recriar os logins
    // public function actionCrialogins(){
    //     $usuarios = Usuario::find()->all();
    //     $cont = 0;
    //     foreach($usuarios as $usuario){
    //         $login = new Logincerebrum();
    //         $login->attributes = $usuario->attributes;
    //         $login->usuario_id = $usuario->id;
    //         $login->user_lvl = Logincerebrum::USUARIO_COMUM;
    //         $login->status_acesso = Logincerebrum::STATUS_ATIVO;
    //         if($login->save()){
    //             $cont++;
    //             echo "<pre>"; print_r("salvou ".$cont);
    //         }else{
    //             echo "<pre>"; print_r("não salvou ".$usuario->cpf);
    //         }
    //     }
        
    //     echo "<pre>"; print_r(count($usuarios));die;
    //     return null;
    // }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
