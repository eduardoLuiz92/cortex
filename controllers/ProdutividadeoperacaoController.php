<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Produtividadeoperacao;
use app\models\ProdutividadeoperacaoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use app\models\Operacao;
use app\models\Orgao;
use app\models\Usuario;
use app\models\Indicador;
use app\models\Localidade;
/**
 * ProdutividadeoperacaoController implements the CRUD actions for Produtividadeoperacao model.
 */
class ProdutividadeoperacaoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'login', 'logout','create','index','delete','update','view','relatorioprodutividadeoperacao',
                    'createajax','getlocalidadeandindicadores','viewrelatorioprodutividadesoperacao'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create','view','index','delete','update','relatorioprodutividadeoperacao',
                        'createajax','getlocalidadeandindicadores','viewrelatorioprodutividadesoperacao'],
                        'matchCallback' => function($rule,$action){
                            if(!Yii::$app->user->isGuest){
                                // echo "<pre>"; print_r(Yii::$app->user->identity->user_level);die;
                                return true;
                                // if(Yii::$app->user->identity->user_lvl_di == 1){
                                //     return true;
                                // }else{
                                //     return false;
                                // }
                            }
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Produtividadeoperacao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProdutividadeoperacaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Produtividadeoperacao model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $model->nome_operacao = Operacao::findOne($model->operacao_id)->nome;
        $model->nome_usuario = Usuario::findOne($model->usuario_id)->username;
        $model->nome_orgao = Orgao::findOne($model->orgao_id)->descricao;

        return $this->render('view-novo', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Produtividadeoperacao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($opid)
    {
        $model = new Produtividadeoperacao();
        $model->operacao_id = $opid;
        $model->usuario_id = Yii::$app->user->identity->usuario_id;
        $model->orgao_id = Usuario::findOne($model->usuario_id)->orgao_id;

        $model->nome_operacao = Operacao::findOne($model->operacao_id)->nome;
        $model->nome_usuario = Yii::$app->user->identity->username;
        $model->nome_orgao = Orgao::findOne($model->orgao_id)->descricao;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionRelatorioprodutividadeoperacao($opid){
        $operacao = Operacao::findOne($opid);
        $relatorio = Produtividadeoperacao::getRelatorioProdutividadeOperacao($operacao->id);
        $total_prods = [];
        return $this->render('relatorio-produtividade', [
            'operacao'=>$operacao,
            'relatorio' => $relatorio,
            'total_prods'=>$total_prods
        ]);
    }

    // testar funcionalidade apois adicionada às rules
    public function actionCreateajax($opid)
    {
        $model = new Produtividadeoperacao();
        $model->operacao_id = $opid;
        $model->usuario_id = Yii::$app->user->identity->usuario_id;
        $model->orgao_id = Usuario::find()->where(['id'=>$model->usuario_id])->one()->orgao_id;
        

        $model->nome_operacao = Operacao::findOne($model->operacao_id)->nome;
        $model->nome_usuario = Yii::$app->user->identity->username;
        $model->nome_orgao = Orgao::findOne($model->orgao_id)->descricao;
        $model->data = date("d/m/Y");

        if ($model->load(Yii::$app->request->post() )){
            if($model->save()){
                return json_encode(['message'=>'salvo com sucesso','titulo'=>'Salvo!','tipo'=>'success','STATUS'=>'sucesso']);
            }else{
                return json_encode(['message'=>'Verifique se os campos de produtividade foram devidamente preenchidos','titulo'=>'Erro','tipo'=>'info','STATUS'=>'erro']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionGetlocalidadeandindicadores($opid){
        $indicadores = [];
        $localidades = [];
        $filtros = [];
        foreach(Indicador::find()->all() as $indicador){
            array_push($indicadores,['id'=>$indicador->id,'descricao'=>$indicador->descricao]);
        }
        foreach(Localidade::find()->where(['operacao_id'=>$opid])->all() as $localidade){
            array_push($localidades,['id'=>$localidade->id,'descricao'=>$localidade->nome]);
        }
        array_push($filtros,['indicadores'=>$indicadores]);
        array_push($filtros,['localidades'=>$localidades]);
        echo json_encode($filtros);
    }

    /**
     * Updates an existing Produtividadeoperacao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->nome_operacao = Operacao::findOne($model->operacao_id)->nome;
        $model->nome_usuario = Usuario::findOne($model->usuario_id)->username;
        $model->nome_orgao = Orgao::findOne($model->orgao_id)->descricao;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionViewrelatorioprodutividadesoperacao($opid){
        $operacao = Operacao::findOne($opid);
        $produtividades = Produtividadeoperacao::getProdutividadeOperacao($opid);

        return $this->render('view-relatorio-produtividade', [
            'operacao' => $operacao,
            'relatorio' => $produtividades
        ]);
    }

    /**
     * Deletes an existing Produtividadeoperacao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return $this->redirect(['index','status'=>'400','message'=>'não foi possivel excluir']);
    }

    /**
     * Finds the Produtividadeoperacao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Produtividadeoperacao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Produtividadeoperacao::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
