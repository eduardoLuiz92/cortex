<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Operacao;
use app\models\OperacaoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Usuario;
use app\models\Localidade;
use app\models\Produtividadeoperacao;
use app\models\Recursooperacao;
use app\models\Relatorio;

/**
 * OperacaoController implements the CRUD actions for Operacao model.
 */
class OperacaoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout','create','index','delete','update','view','viewallprodutividades','viewallrecursos','relatoriogeral'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create','view','index','delete','update','viewallprodutividades','viewallrecursos','relatoriogeral'],
                        'matchCallback' => function($rule,$action){
                            if(!Yii::$app->user->isGuest){
                                // echo "<pre>"; print_r(Yii::$app->user->identity->user_level);die;
                                return true;
                                // if(Yii::$app->user->identity->user_lvl_di == 1){
                                //     return true;
                                // }else{
                                //     return false;
                                // }
                            }
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Operacao models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $searchModel = new OperacaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Operacao model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->nome_aux = Usuario::find()->where(['id'=>$model->usuario_id])->one()->username;

        $produtividades = Produtividadeoperacao::getProdutividadeOperacaoEOrgao(
            $model->id,
            Usuario::findOne(Yii::$app->user->identity->usuario_id)->orgao_id
        );
        
        $localidades = Localidade::getLocalidadesOperacao($model->id);

        return $this->render('view-operacao-com-produtividade', [
            'model' => $model,
            'produtividades'=>$produtividades,
            'localidades' => $localidades
        ]);
    }

    public function actionRelatoriogeral($id){
        $model = $this->findModel($id);
        
        $relatorio = Relatorio::relatorioGeralOperacao($id);

        return $this->renderPartial('../relatorios/relatorio_geral', [
            'model' => $model,
            'relatorio'=>$relatorio
        ]);
    }

    public function actionViewallprodutividades($id){
        $operacao = $this->findModel($id);
        $produtividades = Produtividadeoperacao::find()->where(['operacao_id'=>$id])->all();

        return $this->render('view-all-produtividades', [
            'model' => $operacao,
            'produtividades' => $produtividades
        ]);
    }

    public function actionViewallrecursos($id){
        $model = $this->findModel($id);
        $recursos = Recursooperacao::getAllRecursosOperacao($model->id);

        return $this->render('view-all-recursos', [
            'model' => $model,
            'recursos' => $recursos
        ]);
    }

    /**
     * Creates a new Operacao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Operacao();

        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $model->usuario_id = Yii::$app->user->identity->usuario_id;
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Operacao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        $model->nome_aux = Usuario::find()->where(['id'=>$model->usuario_id])->one()->username;

        // echo "<pre>"; print_r($model->nome_aux);die;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
        
    }

    /**
     * Deletes an existing Operacao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (\Throwable $th) {
            //throw $th;
        }
        return $this->redirect(['index','status'=>'400','message'=>'não foi possivel excluir']);
    }

    /**
     * Finds the Operacao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Operacao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Operacao::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
