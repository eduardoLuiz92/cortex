<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Recursooperacao;
use app\models\RecursooperacaoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Operacao;
use app\models\Orgao;
use app\models\Usuario;
/**
 * RecursooperacaoController implements the CRUD actions for Recursooperacao model.
 */
class RecursooperacaoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'login', 'logout','create','index','delete','update','view','viewrelatoriorecursosoperacao','viewrecursosoperacao'
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create','view','index','delete','update','viewrelatoriorecursosoperacao','viewrecursosoperacao'],
                        'matchCallback' => function($rule,$action){
                            if(!Yii::$app->user->isGuest){
                                // echo "<pre>"; print_r(Yii::$app->user->identity->user_level);die;
                                return true;
                                // if(Yii::$app->user->identity->user_lvl_di == 1){
                                //     return true;
                                // }else{
                                //     return false;
                                // }
                            }
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Recursooperacao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RecursooperacaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Recursooperacao model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $model = $this->findModel($id);
        $operacao = Operacao::findOne($model->operacao_id);
        $total_recursos = Recursooperacao::getTotalrecursos($model->operacao_id);
        return $this->render('view', [
            'model' => $model,
            'operacao' => $operacao,
            'total_recursos' => $total_recursos
        ]);
    }

    public function actionViewrelatoriorecursosoperacao($opid){
        $operacao = Operacao::findOne($opid);
        $recursos = Recursooperacao::getRecursosOperacao($opid);
        $total_recursos = Recursooperacao::getTotalrecursos($opid);
        
        return $this->render('view-relatorio-recursos.php', [
            'operacao' => $operacao,
            'recursos' => $recursos,
            'total_recursos' => $total_recursos
        ]);
    }

    public function actionViewrecursosoperacao($opid,$ogid)
    {   
        
        $operacao = Operacao::findOne($opid);
        $recursos = Recursooperacao::getRecursosPorOrgaoEOperacao($opid,$ogid);
        $total_recursos = Recursooperacao::getTotalRecursosOrgao(
            $operacao->id,
            Usuario::find()->where(['id'=>Yii::$app->user->identity->usuario_id])->one()->orgao_id
        );
        
        return $this->render('view-novo.php', [
            'operacao' => $operacao,
            'recursos' => $recursos,
            'total_recursos'=>$total_recursos
        ]);
    }

    /**
     * Creates a new Recursooperacao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $existe_recurso_preenchido = false;
        $mensagem = "";

        $operacao = Operacao::findOne($id);
        
        $model = new Recursooperacao();
        $model->operacao_id = $id;
        $model->orgao_id = Usuario::find()->where(['id'=>Yii::$app->user->identity->usuario_id])->one()->orgao_id;
        $model->usuario_id = Yii::$app->user->identity->usuario_id;

        $model->nome_usuario_responsavel = Yii::$app->user->identity->username;
        $model->nome_operacao = $operacao->nome;
        $model->nome_orgao_responsavel = Orgao::findOne($model->orgao_id)->descricao;
        
        $total_recursos = Recursooperacao::getTotalRecursosOrgao(
            $operacao->id,$model->orgao_id
        );
        
        if(Yii::$app->request->post()){
            foreach(Yii::$app->request->post()['Recursooperacao'] as $key => $value){
                if($value){
                    if($value > 0){
                        $existe_recurso_preenchido = true;
                    }
                }
                
            }
        }
        if($model->load(Yii::$app->request->post())){
            if($existe_recurso_preenchido == true){
                if ($model->save()) {
                    return $this->redirect(['viewrecursosoperacao', 'opid'=>$model->operacao_id,'ogid'=>$model->orgao_id]);
                }
            }else{
                $mensagem = "Todos os campos estão vazios!";
            }
        }
        

        return $this->render('create', [
            'model' => $model,
            'total_recursos'=>$total_recursos,
            'mensagem' => $mensagem
        ]);
    }

    /**
     * Updates an existing Recursooperacao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $operacao = Operacao::findOne($model->operacao_id);

        $model->nome_usuario_responsavel = Usuario::findOne($model->usuario_id)->username;
        $model->nome_operacao = $operacao->nome;
        $model->nome_orgao_responsavel = Orgao::findOne($model->orgao_id)->descricao;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Recursooperacao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return $this->redirect(['index','status'=>'400','message'=>'não foi possivel excluir']);
    }

    /**
     * Finds the Recursooperacao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Recursooperacao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Recursooperacao::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
