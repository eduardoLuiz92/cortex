<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Localidade;
use app\models\LocalidadeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LocalidadeController implements the CRUD actions for Localidade model.
 */
class LocalidadeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout','index','view','create','update','delete','getlocalidades'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','view','create','update','delete','getlocalidades'],
                        'matchCallback' => function($rule,$action){
                            if(!Yii::$app->user->isGuest){
                                // echo "<pre>"; print_r(Yii::$app->user->identity->user_level);die;
                                return true;
                                // if(Yii::$app->user->identity->user_lvl_di == 1){
                                //     return true;
                                // }else{
                                //     return false;
                                // }
                            }
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Localidade models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LocalidadeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGetlocalidades(){
        $localidades = [];
        foreach(Localidade::find()->all() as $localidade){
            array_push($localidades,['id'=>$localidade->id,'descricao'=>$localidade->nome]);
        }
        return json_encode($localidades);
    }

    /**
     * Displays a single Localidade model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Localidade model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Localidade();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateajax($opid){
        $model = new Localidade();
        $model->operacao_id = $opid;

        // if ($model->load(Yii::$app->request->post()) && $model->save()) {

        //     return $this->redirect(['operacao/view', 'id' => $model->operacao_id]);
        // }

        if($model->load(Yii::$app->request->post()) && $model->save()){
            return json_encode(['message'=>'salvo com sucesso','titulo'=>'Salvo!','tipo'=>'success','STATUS'=>'sucesso']);
        }else{
            return json_encode(['message'=>'Verifique se os campos de <b>Localidade</b> foram devidamente preenchidos','titulo'=>'Erro','tipo'=>'info','STATUS'=>'erro']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }



    /**
     * Updates an existing Localidade model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['operacao/view', 'id' => $model->operacao_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Localidade model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
        } catch (\Throwable $th) {
            //throw $th;
        }
    
        return $this->redirect(['index','status'=>'400','message'=>'não foi possivel excluir']);
    }

    /**
     * Finds the Localidade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Localidade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Localidade::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
