<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Operacao;

/* @var $this yii\web\View */
/* @var $model app\models\Localidade */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="localidade-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group field-localidade-operacao_id">
        <label class="control-label" for="localidade-operacao_id">Operacao ID</label>
        <input type="text" value="<?php echo Operacao::findOne($model->operacao_id)->nome; ?>" id="localidade-operacao_id" class="form-control" name="Localidade[operacao_id]" value="3" disabled="" aria-required="true">
    </div>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'endereco')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
