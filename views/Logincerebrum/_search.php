<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogincerebrumSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logincerebrum-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'usuario_id') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'cpf') ?>

    <?= $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'user_lvl') ?>

    <?php // echo $form->field($model, 'status_acesso') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
