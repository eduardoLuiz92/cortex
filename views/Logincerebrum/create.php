<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Logincerebrum */

$this->title = 'Create Logincerebrum';
$this->params['breadcrumbs'][] = ['label' => 'Logincerebrums', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logincerebrum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-novo', [
        'model' => $model,
        'usuarios' => $usuarios
    ]) ?>

</div>
