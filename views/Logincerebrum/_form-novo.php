<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Usuario;
use app\models\Logincerebrum;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Logincerebrum */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logincerebrum-form">

    <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-12">

            <div class="col-md-6">
                <div class="form-group field-operacao-latitude">
                    <label class="control-label" for="operacao-latitude">Usuario</label>
                    <?php echo Html::dropDownList('usuario_id', $model->usuario_id, 
                        ArrayHelper::map($usuarios, 'id', 'username'),
                        [
                            'prompt'=>'Selecione ',
                            'class'=>"form-control", 
                            'onchange'=>'verificarUsuario(this)'
                        ]); 
                    ?>
                </div>

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                
                <?php echo $form->field($model, 'cpf')->textInput(['maxlength' => true, 'class'=>'form-control cpf']) ?>
                <div class="col-md-12 hide">
                    <div class="col-md-6">
                        <?php //echo $form->field($model, 'cpf')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-info">Consultar</div>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

                <div class="form-group field-operacao-latitude">
                    <label class="control-label" for="operacao-latitude">Tipo de Usuario</label>
                    <?= $form->field($model, 'user_lvl')->dropDownList(Logincerebrum::TIPOS_USUARIOS,['value'=>'COMUM'])->label(false); ?>
                </div>
                <div class="form-group field-operacao-latitude">
                    <label class="control-label" for="operacao-latitude">Acesso</label>
                    <?= $form->field($model, 'status_acesso')->dropDownList(Logincerebrum::STATUS_ACESSO,['value'=>'ATIVO'])->label(false); ?>
                </div>
            </div>
        </div>
        
    
    <?php ActiveForm::end(); ?>

</div>

<script src="../js/jquery-3.3.1.min.js" ></script>
<script src="../js/jquery.maskedinput.min.js" ></script>

<script>
    function verificarUsuario(e){
        
        loading();

        $.ajax({
            url: '<?php echo Url::to(['usuario/getusuario']);?>',
            dataType: 'json',
            type: 'GET',
            contentType: 'application/json',
            data: {'id':e.value},
            success: function (data) {
                console.log('Submission was successful.');
                let retorno = data;
                if(retorno){
                    $('#logincerebrum-username').val(retorno.data.username);
                    $('#logincerebrum-cpf').val(retorno.data.cpf);
                    // console.log(filtros.data.username);
                }
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        }).done(function(){
            encerraLoading();
        });
    }

    $(document).ready(function($){
        $('.cpf').mask("999.999.999-99");        
    })
</script>