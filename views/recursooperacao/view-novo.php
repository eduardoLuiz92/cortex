<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Orgao;
use app\models\Operacao;
use app\models\Usuario;

$this->params['breadcrumbs'][] = ['label' => 'Operação', 'url' => ['operacao/view','id'=>$operacao->id]];
$this->params['breadcrumbs'][] = $this->title;
$contadorLinhas = 1;
?>

<div class="recursooperacao-form">
    <fieldset>
        <legend><h1>Recursos do seu orgão - Operação: <?php echo $operacao->nome; ?></h1></legend>
        <div class="col-md-12">            
            <div class="row">
                <div class="form-group field-operacao-nome required">
                    <label class="control-label" for="operacao-nome">Operação</label>
                    <input type="text" value="<?php echo $operacao->nome; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                </div>
            </div>
            <div class="row">
                <div class="form-group field-operacao-nome required">
                    <label class="control-label" for="operacao-nome">Orgão Reponsável pela operação</label>
                    <input type="text" value="<?php echo Orgao::findOne($operacao->orgao_responsavel_id)->descricao; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                </div>
            </div>
            <div class="row">
                <div class="form-group field-operacao-nome required">
                    <label class="control-label" for="operacao-nome">Responsável pela operação</label>
                    <input type="text" value="<?php echo Usuario::findOne($operacao->usuario_id)->username;  ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                </div>
            </div>
        </div>

        <table class="table table-bordered">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Orgão</th>
                    <th scope="col">Efetivo</th>
                    <th scope="col">VTR 2R</th>
                    <th scope="col">VTR 4R</th>
                    <th scope="col">Aero. asa fixa</th>
                    <th scope="col">Aero. asa móvel</th>
                    <th scope="col">Ambulancia</th>
                    <th scope="col">micro Onibus</th>
                    <th scope="col">HT</th>
                    <th scope="col">Van</th>
                    <th scope="col">Embarcação</th>
                    <th scope="col">Helicoptero</th>
                    <th scope="col">Plataforma</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($total_recursos as $recurso):?>
                        <tr>
                            <th scope="row"><?php echo "TOTAL"; ?></th>
                            <td><?php echo Orgao::findOne($recurso->orgao_id)->sigla;?></td>
                            <td><?php echo $recurso->efetivo;?></td>
                            <td><?php echo $recurso->vtr_2_rodas;?></td>
                            <td><?php echo $recurso->vtr_4_rodas;?></td>
                            <td><?php echo $recurso->aeronave_asa_fixa;?></td>
                            <td><?php echo $recurso->aeronave_asa_movel;?></td>
                            <td><?php echo $recurso->ambulancia;?></td>
                            <td><?php echo $recurso->onibus_microonibus;?></td>
                            <td><?php echo $recurso->ht;?></td>
                            <td><?php echo $recurso->van;?></td>
                            <td><?php echo $recurso->embarcacao;?></td>
                            <td><?php echo $recurso->helicoptero;?></td>
                            <td><?php echo $recurso->plataforma;?></td>
                        </tr>
                        <?php $contadorLinhas++;?>
                    <?php endforeach; ?>
                </tbody>
            </table>    

    
        <div class="col-md-12">
        
        <a onclick = "loading()" href="<?php echo Url::to(['recursooperacao/create','id'=>$operacao->id])?>"><div style="margin: 5px 0px 15px 0px" class="btn btn-success">Adicionar Recursos</div></a>
            
            <div class="panel-group">
                <?php if($recursos):?>
                    <?php foreach($recursos as $recurso):?>
                        <div class="panel panel-primary">
                            <div class="panel-heading"><h4><?php echo Orgao::findOne($recurso->orgao_id)->descricao;?></h4></div>
                            <div class="panel-body">

                            <div class="col-md-6">
                                <?php if($recurso->efetivo):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Efetivo</label>
                                            <input type="text" value="<?php echo $recurso->efetivo; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->vtr_2_rodas):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">VTR 2 Rodas</label>
                                            <input type="text" value="<?php echo $recurso->vtr_2_rodas; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->vtr_4_rodas):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">VTR 4 Rodas</label>
                                            <input type="text" value="<?php echo $recurso->vtr_4_rodas; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->aeronave_asa_fixa):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Aeronave Asa Fixa</label>
                                            <input type="text" value="<?php echo $recurso->aeronave_asa_fixa; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->aeronave_asa_movel):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Aeronave Asa Móvel</label>
                                            <input type="text" value="<?php echo $recurso->aeronave_asa_movel; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->ambulancia):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Ambulância</label>
                                            <input type="text" value="<?php echo $recurso->ambulancia; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                
                            </div>

                            <div class="col-md-6">
                                <?php if($recurso->onibus_microonibus):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Ônibus/Microonibus</label>
                                            <input type="text" value="<?php echo $recurso->onibus_microonibus; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->ht):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">HT</label>
                                            <input type="text" value="<?php echo $recurso->ht; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->van):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Van</label>
                                            <input type="text" value="<?php echo $recurso->van; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->embarcacao):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Embarcação</label>
                                            <input type="text" value="<?php echo $recurso->embarcacao; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->helicoptero):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Helicoptero</label>
                                            <input type="text" value="<?php echo $recurso->helicoptero; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->plataforma):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Plataforma</label>
                                            <input type="text" value="<?php echo $recurso->plataforma; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                            </div>

                            </div>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
        <a onclick="loading()" href="<?php echo Url::to(['operacao/view','id'=>$operacao->id])?>"><div style="margin: 5px 0px 15px 0px" class="btn btn-info">Voltar</div></a>
    </fieldset>
</div>