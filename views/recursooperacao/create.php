<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recursooperacao */

// $this->title = 'Create Recursooperacao';
// $this->params['breadcrumbs'][] = ['label' => 'Recursooperacaos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="recursooperacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-novo', [
        'model' => $model,
        'total_recursos'=>$total_recursos,
        'mensagem' => $mensagem
    ]) ?>

</div>
