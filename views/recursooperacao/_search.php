<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RecursooperacaoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recursooperacao-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'operacao_id') ?>

    <?= $form->field($model, 'orgao_id') ?>

    <?= $form->field($model, 'efetivo') ?>

    <?= $form->field($model, 'vtr_2_rodas') ?>

    <?php // echo $form->field($model, 'vtr_4_rodas') ?>

    <?php // echo $form->field($model, 'aeronave_asa_fixa') ?>

    <?php // echo $form->field($model, 'aeronave_asa_movel') ?>

    <?php // echo $form->field($model, 'ambulancia') ?>

    <?php // echo $form->field($model, 'onibus_microonibus') ?>

    <?php // echo $form->field($model, 'ht') ?>

    <?php // echo $form->field($model, 'van') ?>

    <?php // echo $form->field($model, 'embarcacao') ?>

    <?php // echo $form->field($model, 'helicoptero') ?>

    <?php // echo $form->field($model, 'plataforma') ?>

    <?php // echo $form->field($model, 'usuario_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
