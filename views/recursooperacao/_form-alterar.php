<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Recursooperacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recursooperacao-form">
    
    <?php $form = ActiveForm::begin(); ?>
        <fieldset>
            <legend><h1>Alterar Recurso</h1></legend>
            <div class="col-md-12">
                <div class="col-md-12">            
                    <div class="row">
                        <?= $form->field($model, 'nome_operacao')->textInput(['disabled'=>true]) ?>

                        <?= $form->field($model, 'nome_orgao_responsavel')->textInput(['disabled'=>true]) ?>
                        
                        <?= $form->field($model, 'nome_usuario_responsavel')->textInput(['disabled'=>true]) ?>
                    </div>
                </div>
            

                <div class="col-md-6">
                    <?= $form->field($model, 'efetivo')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'vtr_2_rodas')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'vtr_4_rodas')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'aeronave_asa_fixa')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'aeronave_asa_movel')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'ambulancia')->textInput(['type'=>'number', 'min'=>'0']) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'onibus_microonibus')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'ht')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'van')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'embarcacao')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'helicoptero')->textInput(['type'=>'number', 'min'=>'0']) ?>

                    <?= $form->field($model, 'plataforma')->textInput(['type'=>'number', 'min'=>'0']) ?>
                </div>
            </div>
        </fieldset>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
