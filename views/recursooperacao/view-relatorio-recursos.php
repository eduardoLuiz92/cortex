<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

use app\models\Orgao;

$contadorLinhas = 1;

$this->params['breadcrumbs'][] = ['label' => 'Operação', 'url' => ['operacao/view','id'=>$operacao->id]];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="recursooperacao-form">
    <fieldset>
        <legend><h1>Relatório recursos da Operação: <?php echo $operacao->nome; ?></h1></legend>

        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Orgão</th>
                    <th scope="col">Efetivo</th>
                    <th scope="col">VTR 2R</th>
                    <th scope="col">VTR 4R</th>
                    <th scope="col">Aero. asa fixa</th>
                    <th scope="col">Aero. asa móvel</th>
                    <th scope="col">Ambulancia</th>
                    <th scope="col">micro Onibus</th>
                    <th scope="col">HT</th>
                    <th scope="col">Van</th>
                    <th scope="col">Embarcação</th>
                    <th scope="col">Helicoptero</th>
                    <th scope="col">Plataforma</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($recursos as $recurso):?>
                        <tr>
                            <th scope="row"><?php echo $contadorLinhas?></th>
                            <td><?php echo Orgao::findOne($recurso->orgao_id)->descricao; ?></td>
                            <td><?php echo $recurso->efetivo;?></td>
                            <td><?php echo $recurso->vtr_2_rodas;?></td>
                            <td><?php echo $recurso->vtr_4_rodas;?></td>
                            <td><?php echo $recurso->aeronave_asa_fixa;?></td>
                            <td><?php echo $recurso->aeronave_asa_movel;?></td>
                            <td><?php echo $recurso->ambulancia;?></td>
                            <td><?php echo $recurso->onibus_microonibus;?></td>
                            <td><?php echo $recurso->ht;?></td>
                            <td><?php echo $recurso->van;?></td>
                            <td><?php echo $recurso->embarcacao;?></td>
                            <td><?php echo $recurso->helicoptero;?></td>
                            <td><?php echo $recurso->plataforma;?></td>
                        </tr>
                        <?php $contadorLinhas++;?>
                    <?php endforeach; ?>
                    <?php foreach($total_recursos as $recurso):?>
                        <tr>
                            <th scope="row"><?php echo $contadorLinhas?></th>
                            <td><?php echo "TOTAL"; ?></td>
                            <td><?php echo $recurso->efetivo;?></td>
                            <td><?php echo $recurso->vtr_2_rodas;?></td>
                            <td><?php echo $recurso->vtr_4_rodas;?></td>
                            <td><?php echo $recurso->aeronave_asa_fixa;?></td>
                            <td><?php echo $recurso->aeronave_asa_movel;?></td>
                            <td><?php echo $recurso->ambulancia;?></td>
                            <td><?php echo $recurso->onibus_microonibus;?></td>
                            <td><?php echo $recurso->ht;?></td>
                            <td><?php echo $recurso->van;?></td>
                            <td><?php echo $recurso->embarcacao;?></td>
                            <td><?php echo $recurso->helicoptero;?></td>
                            <td><?php echo $recurso->plataforma;?></td>
                        </tr>
                        <?php $contadorLinhas++;?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <a href="<?php echo Url::to(['operacao/view','id'=>$operacao->id])?>"><div class="btn btn-info">Voltar</div></a>
    </fieldset>
</div>