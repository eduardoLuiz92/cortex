<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Recursooperacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recursooperacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'operacao_id')->textInput() ?>

    <?= $form->field($model, 'orgao_id')->textInput() ?>

    <?= $form->field($model, 'efetivo')->textInput() ?>

    <?= $form->field($model, 'vtr_2_rodas')->textInput() ?>

    <?= $form->field($model, 'vtr_4_rodas')->textInput() ?>

    <?= $form->field($model, 'aeronave_asa_fixa')->textInput() ?>

    <?= $form->field($model, 'aeronave_asa_movel')->textInput() ?>

    <?= $form->field($model, 'ambulancia')->textInput() ?>

    <?= $form->field($model, 'onibus_microonibus')->textInput() ?>

    <?= $form->field($model, 'ht')->textInput() ?>

    <?= $form->field($model, 'van')->textInput() ?>

    <?= $form->field($model, 'embarcacao')->textInput() ?>

    <?= $form->field($model, 'helicoptero')->textInput() ?>

    <?= $form->field($model, 'plataforma')->textInput() ?>

    <?= $form->field($model, 'usuario_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
