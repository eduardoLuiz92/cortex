<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recursooperacao */

// $this->title = 'Update Recursooperacao: ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Recursooperacaos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="recursooperacao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-alterar', [
        'model' => $model,
    ]) ?>

</div>
