<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Orgao;

/* @var $this yii\web\View */
/* @var $model app\models\Recursooperacao */
/* @var $form yii\widgets\ActiveForm */
$this->params['breadcrumbs'][] = ['label' => 'Operação', 'url' => ['operacao/view','id'=>$model->operacao_id]];
?>

<div class="recursooperacao-form">
    
    <?php $form = ActiveForm::begin(); ?>
        <fieldset>
            <legend><h1>Cadastrar Recurso</h1></legend>
            <div class="col-md-12">
                <div class="col-md-12">            
                    <div class="row">
                        <?= $form->field($model, 'nome_operacao')->textInput(['disabled'=>true]) ?>

                        <?= $form->field($model, 'nome_orgao_responsavel')->textInput(['disabled'=>true]) ?>
                        
                        <?= $form->field($model, 'nome_usuario_responsavel')->textInput(['disabled'=>true]) ?>
                    </div>
                </div>
            
                <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Orgão</th>
                        <th scope="col">Efetivo</th>
                        <th scope="col">VTR 2R</th>
                        <th scope="col">VTR 4R</th>
                        <th scope="col">Aero. asa fixa</th>
                        <th scope="col">Aero. asa móvel</th>
                        <th scope="col">Ambulancia</th>
                        <th scope="col">micro Onibus</th>
                        <th scope="col">HT</th>
                        <th scope="col">Van</th>
                        <th scope="col">Embarcação</th>
                        <th scope="col">Helicoptero</th>
                        <th scope="col">Plataforma</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($total_recursos as $recurso):?>
                            <tr>
                                <th scope="row"><?php echo "TOTAL"; ?></th>
                                <td><?php echo Orgao::findOne($model->orgao_id)->sigla;?></td>
                                <td><?php echo $recurso->efetivo;?></td>
                                <td><?php echo $recurso->vtr_2_rodas;?></td>
                                <td><?php echo $recurso->vtr_4_rodas;?></td>
                                <td><?php echo $recurso->aeronave_asa_fixa;?></td>
                                <td><?php echo $recurso->aeronave_asa_movel;?></td>
                                <td><?php echo $recurso->ambulancia;?></td>
                                <td><?php echo $recurso->onibus_microonibus;?></td>
                                <td><?php echo $recurso->ht;?></td>
                                <td><?php echo $recurso->van;?></td>
                                <td><?php echo $recurso->embarcacao;?></td>
                                <td><?php echo $recurso->helicoptero;?></td>
                                <td><?php echo $recurso->plataforma;?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>    
                <fieldset>
                    <legend>Adicionar Recursos</legend>
                        <div class="col-md-6">
                            <?= $form->field($model, 'efetivo')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'vtr_2_rodas')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'vtr_4_rodas')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'aeronave_asa_fixa')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'aeronave_asa_movel')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'ambulancia')->textInput(['type'=>'number', 'min'=>'0']) ?>
                        </div>

                        <div class="col-md-6">
                            <?= $form->field($model, 'onibus_microonibus')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'ht')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'van')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'embarcacao')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'helicoptero')->textInput(['type'=>'number', 'min'=>'0']) ?>

                            <?= $form->field($model, 'plataforma')->textInput(['type'=>'number', 'min'=>'0']) ?>
                        </div>
                </fieldset>
                
            </div>
        </fieldset>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success','onclick'=>'loading()']) ?>
        <a onclick="loading()" href="<?php echo Url::to(['operacao/view','id'=>$model->operacao_id])?>"><div class="btn btn-info">Voltar</div></a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script src="../js/jquery-3.3.1.min.js" ></script>

<script>
    $(document).ready(function(){
        let mensagem = "<?php echo $mensagem;?>";
        verificamensagem(mensagem);
    } );
    
</script>