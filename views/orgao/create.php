<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orgao */

$this->title = 'Cadastrar Orgão';
$this->params['breadcrumbs'][] = ['label' => 'Orgãos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orgao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
