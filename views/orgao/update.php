<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Orgao */

$this->title = 'Alterar Orgao: ' . $model->sigla;
$this->params['breadcrumbs'][] = ['label' => 'Orgaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="orgao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
