<?php

  use app\models\Relatorio;  

  $dados = null;
  if($relatorio){
    $dados = $relatorio;
    $operacao = $dados["operacao"];
    $orgaos = $dados["orgaos"];
    $localidades = $dados["localidades"];
    $indicadores = $dados["indicadores"];
    $recursos = $dados["recursos"][0];
    $produtividades = $dados["produtividades"];
  }

  Relatorio::pdfRelatorio($dados, $model->id);

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8'>
  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
  <title> CEREBRUM - Relatório Geral </title>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
  <script src='main.js'></script>
</head>
<header>
  <div class="cabecalho">
    <img src="../images/logo do estado.png" alt="">
  </div>
  <div class="titulo">
    <H2><?php echo strtoupper($operacao['nome']) ?></H2>
  </div>
</header>


<body>
  <section>
    <div class="topico">
      <H3><?php echo mb_strtoupper("1) Dados Gerais", mb_internal_encoding()); ?></H3>
    </div>

    <div class="topico-content">
      <ul>
        <li>
          <b>Datas: </b> <?php echo $operacao['dt_referencia'];?>
        </li> 
        <li>
          <b>Horário:</b> <?php echo $operacao['hr_referencia']?>
        </li>
        <li>
          <b>Órgãos Participantes: </b>
            <ul>
              <?php foreach ($orgaos as $key => $value): ?>
                <li><?php echo $value; ?></li>
              <?php endforeach; ?> 
            </ul>
        </li>
      </ul>
    </div>
  </section>
  
  <section>
    <div class="topico">
      <H3><?php echo mb_strtoupper("2) Locais: ", mb_internal_encoding()); ?><?php echo count($localidades); ?></H3>
    </div>
    <div class="topico-content">
      <ul>
      <?php foreach ($localidades as $key => $value): ?>
        <li> <b><?php echo $value['nome']; ?></b>
          <ul> <?php echo $value['endereco']; ?> </ul>
        </li>
      <?php endforeach; ?> 
      </ul>
    </div>
  </section>

  <section>
    <div class="topico">
      <H3><?php echo mb_strtoupper("3) Produtividade Geral", mb_internal_encoding()); ?></H3>
    </div>
    <div class="topico-content">
      <ul>
      <?php foreach ($recursos as $key => $value): ?>
        <?php if($value != 0): ?>
          <li> <b><?php echo $key; ?></b>: <?php echo $value; ?></li>
        <?php endif; ?>
      <?php endforeach; ?> 
      </ul>
    </div>
  </section>
  
  <section>
    <div class="topico">
      <H3><?php echo mb_strtoupper("4) recursos Gerais", mb_internal_encoding()); ?></H3>
    </div>
    <div class="topico-content">
      <ul>
        <?php foreach ($indicadores as $key => $value): ?>
          <li> <b><?php echo $value['descricao']; ?></b>: <?php echo $value['qtd']; ?></li>
        <?php endforeach; ?> 
      </ul>
    </div>
  </section>

  <section>
    <div class="topico">
      <H3><?php echo mb_strtoupper("5) Detalhes das Ações por Local", mb_internal_encoding()); ?></H3>
    </div>
    <div class="topico-content">
      <ul>
        <?php foreach ($produtividades as $key_local => $prods): ?>
          <li> <b><?php echo mb_strtoupper($key_local, mb_internal_encoding()); ?></b>: 
            <ul>
              <?php foreach ($prods as $key => $value): ?>
                <li> <b><?php echo $value["Órgão"]." - ";?>  <?php echo $value["Indicador"]."(".$value["Quantidade"].")"; ?></b>: 
                  <ul>
                    <li>
                      Descrição:
                      <?php echo $value["Descrição"];?>
                    </li>
                  </ul>
                </li>
              <?php endforeach; ?> 
            </ul> 
          </li>
        <?php endforeach; ?> 
      </ul>
    </div>
  </section>
  <section class="assinatura">
    <div class="data">
      <p>Manaus, <?php echo date("d")." de ". Relatorio::mes(strval(date("m"))) ." de ". date("Y");?>
    </div>
    <div class="secretario_adjunto">
        <strong>HERMES SILVA DE MACEDO - CEL QOPM</strong>
        <p>Secretário Executivo Adjunto de Planejamento e Gestão Integrada de Segurança</p>
    </div>                
  </section>
  
</body>
</html>

<style>

  .assinatura{
    position: relative;
    bottom: 0;
  }

  .secretario_adjunto {
    text-align: center;
  }

  .data{
    text-align: right;
    margin: 20px;
    margin-top: 40px;
    margin-right: 40px;
    margin-bottom: 40px;
  }

  section{
    max-width: 900px;
    margin: 0 auto;
  }

  body{
    margin-top: 20px;
    margin-bottom: 20px;
    margin-left: 5em;
    margin-right: 5em;
  }

  .cabecalho{
    text-align: center;
  }

  .titulo{
    text-align: center;
    text-decoration: underline;
    margin-bottom: 40px;
  }

  .topico{
    margin-left: 2em;
    margin-top: 10px;
    margin-bottom: 20px;
    text-align: justify;
  }

  .topico-content{
    margin-left: 3em;
  }
  .topico-content > ul > li{
    font-size: 16px;
    margin: 10px;
  }
  .topico-content > ul > li > ul >li{
    margin: 7px;
    margin-bottom: 15px;
    font-size: 14px;
  }

</style>