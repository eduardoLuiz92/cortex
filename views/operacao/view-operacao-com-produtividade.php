<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Orgao;
use yii\helpers\Url;
use app\models\Produtividadeoperacao;
use app\models\Indicador;
use app\models\Usuario;
use app\models\Localidade;
use app\models\Municipio;
use app\models\Logincerebrum;


/* @var $this yii\web\View */
/* @var $model app\models\Operacao */
/* @var $form yii\widgets\ActiveForm */

$produtividadeNova = new Produtividadeoperacao();
$localidadeNova = new Localidade();

// $this->title = 'Create Operacao';
$this->params['breadcrumbs'][] = ['label' => 'Operações', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    a{

    }
</style>

<div class="operacao-form">
    <?php $form = ActiveForm::begin(); ?>
        <fieldset>
            <legend><h1>Operação: <?php echo $model->nome;?></h1></legend>

            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <?= $form->field($model, 'nome')->textInput(['maxlength' => true,'disabled'=>true]) ?>     
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'municipio_desc')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                        <?= $form->field($model, 'municipio_id')
                            ->dropDownList(
                                ArrayHelper::map(Municipio::find()->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Operacao[municipio_id]','disabled'=>true]    // options
                        ); ?>
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'orgao_responsavel_id')->textInput() ?>
                        <?= $form->field($model, 'orgao_responsavel_id')
                            ->dropDownList(
                                ArrayHelper::map(Orgao::find()->all(), 'id', 'sigla'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Operacao[orgao_responsavel_id]','disabled'=>true]    // options
                        ); ?>
                    </div>
                    <div class="row">

                    </div>
                    
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <?= $form->field($model, 'nome_aux')->textInput(['disabled'=>true,'disabled'=>true]) ?>     
                        
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'dt_referencia')->textInput(['maxlength' => true]) ?>
                        <div class="form-group field-operacao-dt_referencia">
                            <label class="control-label" for="operacao-dt_referencia">Data Referência</label>
                            <input type="text" disabled='disabled' value="<?php echo $model->dt_referencia; ?>" id="operacao-dt_referencia" class="data form-control" name="Operacao[dt_referencia]" maxlength="45" aria-invalid="false">
                        </div>
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'hr_referencia')->textInput(['maxlength' => true]) ?>                     
                        <div class="form-group field-operacao-hr_referencia ">
                            <label class="control-label" for="operacao-hr_referencia">Hora Referência</label>
                            <input type="text" disabled='disabled' value="<?php echo $model->hr_referencia; ?>" id="operacao-hr_referencia" class=" hora form-control" name="Operacao[hr_referencia]" maxlength="45" aria-invalid="false">
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                    </div>
                </div>
            </div>
        </fieldset>
        <?php ActiveForm::end(); ?>
        <p>
            <?php //if(Usuario::findOne(Yii::$app->user->identity->usuario_id)->orgao_id == 23):?>
            <?php if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM):?>
                <a target='_blank' href= "https://tinyurl.com/y65ck7uh"><button class="btn btn-primary">DashBoard</button></a>
                <a target='_blank' href= "<?php echo Url::to(['operacao/relatoriogeral','id'=>$model->id])?>"><button class="btn btn-warning">Relatorio Geral</button></a>
                <a target='_blank' href= "<?php echo Url::to(['produtividadeoperacao/viewrelatorioprodutividadesoperacao','opid'=>$model->id])?>"><button class="btn btn-primary">Relatorio Produtividades</button></a>
                <a target='_blank' href= "<?php echo Url::to(['recursooperacao/viewrelatoriorecursosoperacao','opid'=>$model->id])?>"><button class="btn btn-primary">Relatorio Recursos</button></a>
                <a target='_blank' href= "<?php echo Url::to(['operacao/viewallprodutividades','id'=>$model->id])?>"><button class="btn btn-primary">Listar Produtividades</button></a>
                <a target='_blank' href= "<?php echo Url::to(['operacao/viewallrecursos','id'=>$model->id])?>"><button class="btn btn-primary">Listar Recursos</button></a>
                
            <?php endif;?>

            <?php if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM_ORGAO):?>
                <a target='_blank' href= "<?php echo Url::to(['produtividadeoperacao/viewrelatorioprodutividadesoperacao','opid'=>$model->id])?>"><button class="btn btn-primary">Relatorio Produtividades</button></a>
                <a target='_blank' href= "<?php echo Url::to(['recursooperacao/viewrelatoriorecursosoperacao','opid'=>$model->id])?>"><button class="btn btn-primary">Relatorio Recursos</button></a>
            <?php endif; ?>
            
            <a onclick='loading()' href= "<?php echo Url::to(['recursooperacao/viewrecursosoperacao','opid'=>$model->id,'ogid'=>Usuario::findOne(Yii::$app->user->identity->usuario_id)->orgao_id])?>"><button class="btn btn-primary">Recursos do seu órgão</button></a>
            <a onclick='loading()' href= "<?php echo Url::to(['recursooperacao/create','id'=>$model->id])?>"><button class="btn btn-success">Cadastrar Recurso</button></a>
            <!-- <a href= "<?php //echo Url::to(['produtividadeoperacao/create','opid'=>$model->id])?>"><button class="btn btn-primary">Cadastrar produtividade</button></a> -->

            <?php //if(Yii::$app->user->identity->usuario_id == $model->usuario_id):?>
            <?php if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM):?>
            
                <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary','onclick'=>'loading()']) ?>
                <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Tem certeza que deseja excluir esse registro?',
                        'method' => 'post',
                    ],
                ]) ?>
        </p>
    <?php endif;?>
</div>

<div class="col-md-12">
    <fieldset>
        <legend><h1>Produtividade do seu órgão</h1></legend>

        <div class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">Cadastrar produtividade | 
                    <div class="btn btn-success" onclick="atualizarFiltrosProdutividade();">Atualizar indicadores e localidades</div>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(
                        ['options' => [
                            'id' => "produtividade-form",
                            'action'=> Url::to(['produtividadeoperacao/createajax','opid'=>$model->id]),
                            'enctype' => 'multipart/form-data']
                        ]); 
                    ?>  
                        <div class="col-md-9">  
                            <div class="row">
                                <div class=col-md-4>
                                    <div class="form-group field-operacao-latitude">
                                        <label class="control-label" for="operacao-latitude">Indicador</label>
                                        <?php echo Html::dropDownList('Produtividadeoperacao[indicador_id]', $produtividadeNova->indicador_id, ArrayHelper::map(Indicador::find()->all(), 'id', 'descricao'),['prompt'=>'Selecione ','name'=>'Produtividadeoperacao[indicador_id]','id'=>'produtividadeoperacao_indicadores','class'=>"form-control"]); ?>
                                    </div>
                                    
                                </div>
                                <div class=col-md-4>
                                    <?= $form->field($produtividadeNova, 'localidade_id')   
                                        ->dropDownList(
                                            ArrayHelper::map(Localidade::find()->where(['operacao_id'=>$model->id ])->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                            ['prompt'=>'Selecione ','name'=>'Produtividadeoperacao[localidade_id]','id'=>'produtividadeoperacao_localidades']    // options
                                    ); ?>                                
                                </div>
                                <div class=col-md-4>
                                    <?php //echo $form->field($produtividadeNova, 'anexo')->fileInput(['class'=>'form-control']) ?>
                                    <div class="form-group field-operacao-latitude">
                                        <label class="control-label" for="operacao-latitude">Quantidade</label>
                                        <?php echo Html::tag('input', $produtividadeNova->qtd,['name'=>'Produtividadeoperacao[qtd]','type'=>'number','min'=>0,'class'=>'form-control']); ?>
                                    </div>
                                    <div class=" hide form-group field-operacao-latitude">
                                        <label class="control-label" for="operacao-latitude">Hora</label>
                                        <?php echo Html::tag('input', $produtividadeNova->qtd,['name'=>'Produtividadeoperacao[hora]','type'=>'text','class'=>'hora form-control', 'id'=>'produtividade-hora','readonly'=>true]); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($produtividadeNova, 'descricao')->textarea(['rows' => '4']) ?>     
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                
                                <div class=col-md-3>
                                    <div class="form-group field-operacao-latitude">
                                        
                                        <label class="control-label" for="operacao-latitude">ações</label><br>
                                        <a><div class="btn btn-primary" onclick="salvarProdutividade();">Salvar</div></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                        
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

        <?php if($produtividades):?>
            <?php foreach($produtividades as $prod): ?>
                <a target='_blank' href="<?php echo Url::to(['produtividadeoperacao/view','id'=>$prod->id])?>">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                
                                <p>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <?php echo Orgao::findOne($prod->orgao_id)->descricao;?> | 
                                            <?php echo Usuario::findOne($prod->usuario_id)->username;?> |
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <b>
                                                <?php echo Indicador::findOne($prod->indicador_id)->descricao;?> | 
                                                <?php echo Localidade::findOne($prod->localidade_id)->nome;?>
                                            </b> 
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <b>Qtd: </b><?php echo $prod->qtd;?>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <?php echo $prod->data." - ".$prod->hora;?> 
                                        </div>
                                    </div>
                                    <br> 
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
            <?php endforeach;?>
        <?php endif;?>
        
    </fieldset>
</div>

<div class="col-md-12">
    <fieldset>
        <legend><h1>Localidades da Operação</h1></legend>
        <!-- só usuários vinculados ao CICC podem cadastrar Localidades -->
        <?php if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM):?>   
            <div class="panel-group">
                <div class="panel panel-primary">
                    <div class="panel-heading">Cadastrar localidade</div>
                    <div class="panel-body">
                        <form id="localidade-form" action="<?=Url::to(['localidade/createajax','opid'=>$model->id])?>" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class=col-md-6>
                                        <div class="form-group field-operacao-latitude">
                                            <label class="control-label" for="operacao-latitude">Endereço</label>
                                            <?php echo Html::tag('input', $localidadeNova->endereco,['name'=>'Localidade[endereco]','type'=>'text','class'=>'form-control']); ?>
                                        </div>
                                    </div>    
                                    <div class="col-md-6">
                                        <div class=col-md-6>
                                            <div class="form-group field-operacao-latitude">
                                                <label class="control-label" for="operacao-latitude">Nome do local</label>
                                                <?php echo Html::tag('input', $localidadeNova->nome,['name'=>'Localidade[nome]','type'=>'text','class'=>'form-control']); ?>
                                            </div>
                                        </div>
                                        <div class=col-md-6>
                                            <div class="form-group field-operacao-latitude">
                                                <label class="control-label" for="operacao-latitude">ações</label><br>
                                                <a><div class="btn btn-primary" onclick="salvarLocalidade();">Salvar</div></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <?php if($localidades):?>
            <?php foreach($localidades as $localidade): ?>
                <div class="panel-group">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <p>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <b>Endereço: </b><?php echo $localidade->endereco;?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            <b>Local: </b><?php echo $localidade->nome;?> 
                                        </div>
                                        <?php //if(Usuario::findOne(Yii::$app->user->identity->usuario_id)->orgao_id == 23):?>   
                                        <?php if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM):?>   
                                            <div class="col-md-6">
                                                <a href= "<?php echo Url::to(['localidade/update','id'=>$localidade->id])?>"><button class="btn btn-danger">alterar</button></a>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                </div>
                            <br> 
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        <?php endif;?>
        
        
    </fieldset>
    

</div>


<script src="../js/jquery-3.3.1.min.js" ></script>
<script src="../js/jquery.maskedinput.min.js" ></script>
<script>
    let filtros = null;
    // function loading(){
    //     swal({
    //         title: "Loading...",
    //         id:'load-modal',
    //         onOpen: () => {
    //             swal.showLoading()
    //             timerInterval = setInterval(() => {
    //             },100)
    //         },
    //     });
    // }

    // function encerraLoading(){
    //     swal.close();
    // }

    function salvarProdutividade(){
        
        let retorno = "";
        loading();

        $('#produtividade-hora').val(dayjs().format("HH:mm"))
        
        var frm = $('#produtividade-form');

        $.ajax({
            type: frm.attr('method'),
            url: '<?php echo Url::to(['produtividadeoperacao/createajax','opid'=>$model->id]);?>',
            data: frm.serialize(),
            success: function (data) {
                retorno = JSON.parse(data);
                console.log('Submission was successful.');
                console.log(data);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        }).done(function(data){
            if(retorno.STATUS == 'erro'){
                sweetAlert(retorno.titulo, retorno.message ,retorno.tipo);
            }else{
                location.reload();
            }
            
            
        });

    }

    function salvarLocalidade(){
        let retorno = "";
        loading();

        var frm = $('#localidade-form');

        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action'),
            data: frm.serialize(),
            success: function (data) {
                retorno = JSON.parse(data);
                console.log('Submission was successful.');
                console.log(data);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        }).done(function(){
            if(retorno.STATUS == 'erro'){
                sweetAlert(retorno.titulo, retorno.message ,retorno.tipo);
            }else{
                location.reload();
            }
        });

    }

    function atualizarFiltroIndicadores(filtros){
        $("#produtividadeoperacao_indicadores").html('');
        $('#produtividadeoperacao_indicadores').append('<option value="">Selecione </option>');
        filtros.forEach(e => {
            $('#produtividadeoperacao_indicadores').append('<option value='+e.id+'>'+e.descricao+'</option>');
        });
        
    }

    function atualizarFiltroLocalidades(filtros){
        $("#produtividadeoperacao_localidades").html('');
        $('#produtividadeoperacao_localidades').append('<option value="">Selecione </option>');
        filtros.forEach(e => {
            $('#produtividadeoperacao_localidades').append('<option value='+e.id+'>'+e.descricao+'</option>');
        });
        
    }

    function atualizarFiltrosProdutividade(){
        
        loading();

        $.ajax({
            type: 'GET',
            url: '<?php echo Url::to(['produtividadeoperacao/getlocalidadeandindicadores','opid'=>$model->id]);?>',
            data: '',
            success: function (data) {
                console.log('Submission was successful.');
                filtros = JSON.parse(data);
                console.log(filtros);
                atualizarFiltroIndicadores(filtros[0].indicadores);
                atualizarFiltroLocalidades(filtros[1].localidades);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        }).done(function(){
            encerraLoading();
        });
    }

    $(document).ready(function($){
        $('.data').mask("99/99/9999");
        $('.hora').mask("99:99");
        
    })
</script>
