<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Orgao;
use app\models\Operacao;
use app\models\Usuario;
use app\models\Logincerebrum;

$this->params['breadcrumbs'][] = ['label' => 'Operação', 'url' => ['operacao/view','id'=>$model->id]];
$this->params['breadcrumbs'][] = $this->title;
$contadorLinhas = 1;
?>

<div class="recursooperacao-form">
    <fieldset>
        <legend><h1>Recursos do seu orgão - Operação: <?php echo $model->nome; ?></h1></legend>
        <div class="col-md-12">            
            <div class="row">
                <div class="form-group field-operacao-nome required">
                    <label class="control-label" for="operacao-nome">Operação</label>
                    <input type="text" value="<?php echo $model->nome; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                </div>
            </div>
            <div class="row">
                <div class="form-group field-operacao-nome required">
                    <label class="control-label" for="operacao-nome">Orgão Reponsável pela operação</label>
                    <input type="text" value="<?php echo Orgao::findOne($model->orgao_responsavel_id)->descricao; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                </div>
            </div>
            <div class="row">
                <div class="form-group field-operacao-nome required">
                    <label class="control-label" for="operacao-nome">Responsável pela operação</label>
                    <input type="text" value="<?php echo Usuario::findOne($model->usuario_id)->username;  ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                </div>
            </div>
        </div>

    
        <div class="col-md-12">
        
        <a onclick = "loading()" href="<?php echo Url::to(['recursooperacao/create','id'=>$model->id])?>"><div style="margin: 5px 0px 15px 0px" class="btn btn-success">Adicionar Recursos</div></a>
            
            <div class="panel-group">
                <?php if($recursos):?>
                    <?php foreach($recursos as $recurso):?>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4><?php echo Orgao::findOne($recurso->orgao_id)->descricao;?></h4>
                                <?php if(Usuario::findOne(Yii::$app->user->identity->usuario_id)->orgao_id == 23): ?>
                                    <?php if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM): ?>
                                        <a href="<?php echo Url::to(['recursooperacao/update','id'=>$recurso->id])?>" ><div class="btn btn-warning">Alterar</div></a>
                                    <?php endif;?>
                                <?php endif;?>
                            </div>
                            <div class="panel-body">

                            <div class="col-md-6">
                                <?php if($recurso->efetivo):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Efetivo</label>
                                            <input type="text" value="<?php echo $recurso->efetivo; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->vtr_2_rodas):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">VTR 2 Rodas</label>
                                            <input type="text" value="<?php echo $recurso->vtr_2_rodas; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->vtr_4_rodas):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">VTR 4 Rodas</label>
                                            <input type="text" value="<?php echo $recurso->vtr_4_rodas; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->aeronave_asa_fixa):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Aeronave Asa Fixa</label>
                                            <input type="text" value="<?php echo $recurso->aeronave_asa_fixa; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->aeronave_asa_movel):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Aeronave Asa Móvel</label>
                                            <input type="text" value="<?php echo $recurso->aeronave_asa_movel; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->ambulancia):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Ambulância</label>
                                            <input type="text" value="<?php echo $recurso->ambulancia; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                
                            </div>

                            <div class="col-md-6">
                                <?php if($recurso->onibus_microonibus):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Ônibus/Microonibus</label>
                                            <input type="text" value="<?php echo $recurso->onibus_microonibus; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->ht):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">HT</label>
                                            <input type="text" value="<?php echo $recurso->ht; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->van):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Van</label>
                                            <input type="text" value="<?php echo $recurso->van; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->embarcacao):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Embarcação</label>
                                            <input type="text" value="<?php echo $recurso->embarcacao; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->helicoptero):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Helicoptero</label>
                                            <input type="text" value="<?php echo $recurso->helicoptero; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                                <?php if($recurso->plataforma):?>
                                    <div class="row">
                                        <div class="form-group field-operacao-nome required">
                                            <label class="control-label" for="operacao-nome">Plataforma</label>
                                            <input type="text" value="<?php echo $recurso->plataforma; ?>" id="operacao-nome" class="form-control" name="Operacao[nome]"  disabled="" maxlength="100" aria-required="true">
                                        </div>
                                    </div>
                                <?php endif;?>
                            </div>

                            </div>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
        </div>
        <a onclick="loading()" href="<?php echo Url::to(['operacao/view','id'=>$model->id])?>"><div style="margin: 5px 0px 15px 0px" class="btn btn-info">Voltar</div></a>
    </fieldset>
</div>