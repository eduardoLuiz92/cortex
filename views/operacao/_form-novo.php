<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Orgao;
use app\models\Municipio;


/* @var $this yii\web\View */
/* @var $model app\models\Operacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacao-form">
    <?php $form = ActiveForm::begin(); ?>
        <fieldset>
            <legend><h1>Cadastrar Operação</h1></legend>

            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>     
                    </div>
                    <div class="row">
                        <?= $form->field($model, 'municipio_id')
                            ->dropDownList(
                                ArrayHelper::map(Municipio::find()->all(), 'id', 'nome'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Operacao[municipio_id]']    // options
                        ); ?>
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'orgao_responsavel_id')->textInput() ?>
                        <?= $form->field($model, 'orgao_responsavel_id')
                            ->dropDownList(
                                ArrayHelper::map(Orgao::find()->all(), 'id', 'sigla'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Operacao[orgao_responsavel_id]']    // options
                        ); ?>
                    </div>
                    
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <?= $form->field($model, 'usuario_id')->textInput(['readOnly'=>true,'value'=>Yii::$app->user->identity->username]) ?>     
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'dt_referencia')->textInput(['maxlength' => true]) ?>
                        <div class="form-group field-operacao-dt_referencia">
                            <label class="control-label" for="operacao-dt_referencia">Data Referência</label>
                            <input type="text" id="operacao-dt_referencia" class="data form-control" name="Operacao[dt_referencia]" maxlength="45" aria-invalid="false">
                        </div>
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'hr_referencia')->textInput(['maxlength' => true]) ?>                     
                        <div class="form-group field-operacao-hr_referencia ">
                            <label class="control-label" for="operacao-hr_referencia">Hora Referência</label>
                            <input type="text" id="operacao-hr_referencia" class=" hora form-control" name="Operacao[hr_referencia]" maxlength="45" aria-invalid="false">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">

                </div>
                
            </div>
        </fieldset>

    <?php //echo $form->field($model, 'municipio_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script src="../js/jquery-3.3.1.min.js" ></script>
<script src="../js/jquery.maskedinput.min.js" ></script>
<script>
    $(document).ready(function($){
        $('.data').mask("99/99/9999");
        $('.hora').mask("99:99");
    })
</script>