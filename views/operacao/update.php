<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Operacao */

// $this->title = 'Update Operacao: ' . $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Operacaos', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="operacao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-alterar', [
        'model' => $model,
    ]) ?>

</div>
