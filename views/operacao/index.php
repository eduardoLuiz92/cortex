<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Municipio;
use app\models\Orgao;
use app\models\Logincerebrum;
use app\models\Usuario;


/* @var $this yii\web\View */
/* @var $searchModel app\models\OperacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Operações';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacao-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if(!Yii::$app->user->isGuest):?>
            <?php if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM):?>
                <?= Html::a('Cadastrar Operação', ['create'], ['class' => 'btn btn-success']) ?>
            <?php endif;?>
        <?php endif;?>
    </p>
<?php

    if(!Yii::$app->user->isGuest){
        if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_COMUM){
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'contentOptions' => ['style' => 'width: 20px;', 'class' => 'text-center'],
                    ],
                    [
                        'attribute' => 'nome',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => ['style' => 'width: 70%; color:#337ab7;', 'class' => 'text-left'],
                    ],
                    // [
                    //     'attribute' => 'dt_referencia',
                    //     'headerOptions' => ['class' => 'text-center'],
                    //     'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                    // ],
                    [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'Ações', 
                    'headerOptions' => ['width' => '80','style'=>'width: 30%; color:#337ab7;', 'class'=>'text-center'],
                    'template' => '{view} {alterar} {deletar}',
                    'buttons' => [
                            'view' => function($url,$model,$key){
                                    return Html::a('<div class="btn btn-info" onclick="loading()">Visualizar</div>', Url::to(['operacao/view','id'=>$key]));
                                
                            },
                            // 'alterar' => function($url,$model,$key){
                            //     if(Usuario::findOne(Yii::$app->user->identity->usuario_id)->orgao_id == 23){
                            //         return Html::a('<div class="btn btn-warning" onclick="loading()">alterar</div>', Url::to(['operacao/update','id'=>$key]));
                            //     }
                            // },
                            // 'deletar' => function($url,$model,$key){
                            //     if(Yii::$app->user->identity->id == $model->usuario_id){
                            //         return Html::a('<div class="btn btn-danger" onclick="loading()">Deletar</div>', Url::to(['#']));
                            //     }
                            // },
                        ],
                    ],
                ],
            ]); 
        }else{
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'contentOptions' => ['style' => 'width: 20px;', 'class' => 'text-center'],
                    ],
                    [
                        'attribute' => 'nome',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                    ],
                    [
                        'attribute' => 'orgao_responsavel_id',
                        'headerOptions' => ['class' => 'text-center'],
                        'value'  => function ($data) {
                            return Orgao::findOne($data->orgao_responsavel_id)->sigla;
                        },
                        'contentOptions' => ['style' => 'width: 10%; color:#337ab7;', 'class' => 'text-left'],
                    ],
                    // [
                    //     'attribute' => 'municipio_id',
                    //     'headerOptions' => ['class' => 'text-center'],
                    //     'value'  => function ($data) {
                    //         return Municipio::findOne($data->municipio_id)->nome;
                    //     },
                    //     'contentOptions' => ['style' => 'width: 10%; color:#337ab7;', 'class' => 'text-left'],
                    // ],
                    [
                        'attribute' => 'dt_referencia',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
                    ],
                    [
                        'attribute' => 'hr_referencia',
                        'headerOptions' => ['class' => 'text-center'],
                        'contentOptions' => ['style' => 'width: 5%; color:#337ab7;', 'class' => 'text-left'],
                    ],    
                    [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'Ações', 
                    'headerOptions' => ['width' => '80','style'=>'width: 30%; color:#337ab7;', 'class'=>'text-center'],
                    'template' => '{view} {alterar} {relatorio} {deletar}',
                    'buttons' => [
                            'view' => function($url,$model,$key){
                                    return Html::a('<div class="btn btn-info" onclick="loading()">Visualizar</div>', Url::to(['operacao/view','id'=>$key]));
                                
                            },
                            'alterar' => function($url,$model,$key){
                                if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM){
                                    return Html::a('<div class="btn btn-warning" onclick="loading()">alterar</div>', Url::to(['operacao/update','id'=>$key]));
                                    return Html::a('<div class="btn btn-warning" onclick="loading()">alterar</div>', Url::to(['operacao/relatoriogeral','id'=>$key]));
                                }
                                // else if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM_ORGAO && 
                                //          Yii::$app->user->identity->usuario_id == $model->usuario_id){
                                //     return Html::a('<div class="btn btn-warning" onclick="loading()">alterar</div>', Url::to(['operacao/update','id'=>$key]));
                                // }
                            },
                            'relatorio' => function($url,$model,$key){
                                if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM){
                                    return Html::a('<div title="Relatório Geral" class="btn btn-success"><span class="glyphicon glyphicon-file"></span></div>', Url::to(['operacao/relatoriogeral','id'=>$key]),['target'=>'_blank']);
                                }
                            }
                        ],
                    ],
                ],
            ]); 
        }
    }
        

        
    ?>
</div>
