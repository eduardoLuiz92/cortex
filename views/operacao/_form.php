<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Operacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'municipio_desc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'municipio_id')->textInput() ?>

    <?= $form->field($model, 'orgao_responsavel_id')->textInput() ?>

    <?= $form->field($model, 'dt_referencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hr_referencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
