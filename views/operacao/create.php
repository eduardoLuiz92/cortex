<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Operacao */

// $this->title = 'Create Operacao';
$this->params['breadcrumbs'][] = ['label' => 'Operações', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="operacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-novo', [
        'model' => $model,
    ]) ?>

</div>
