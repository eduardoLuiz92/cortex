<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Orgao;
use app\models\Usuario;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Operacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacao-form">
    <?php $form = ActiveForm::begin(); ?>
        <fieldset>
            <legend><h1>Operação: <?php echo $model->nome;?></h1></legend>

            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <?= $form->field($model, 'nome')->textInput(['maxlength' => true,'disabled'=>true]) ?>     
                    </div>
                    <div class="row">
                        <?= $form->field($model, 'municipio_desc')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'orgao_responsavel_id')->textInput() ?>
                        <?= $form->field($model, 'orgao_responsavel_id')
                            ->dropDownList(
                                ArrayHelper::map(Orgao::find()->all(), 'id', 'sigla'),         // Flat array ('id'=>'label')
                                ['prompt'=>'Selecione ','name'=>'Operacao[orgao_responsavel_id]','disabled'=>true]    // options
                        ); ?>
                    </div>
                    <div class="row">
                    </div>
                    
                </div>

                <div class="col-md-6">
                    <div class="row">

                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'dt_referencia')->textInput(['maxlength' => true]) ?>
                        <div class="form-group field-operacao-dt_referencia">
                            <label class="control-label" for="operacao-dt_referencia">Data Referência</label>
                            <input type="text" disabled='disabled' value="<?php echo $model->dt_referencia; ?>" id="operacao-dt_referencia" class="data form-control" name="Operacao[dt_referencia]" maxlength="45" aria-invalid="false">
                        </div>
                    </div>
                    <div class="row">
                        <?php //echo $form->field($model, 'hr_referencia')->textInput(['maxlength' => true]) ?>                     
                        <div class="form-group field-operacao-hr_referencia ">
                            <label class="control-label" for="operacao-hr_referencia">Hora Referência</label>
                            <input type="text" disabled='disabled' value="<?php echo $model->hr_referencia; ?>" id="operacao-hr_referencia" class=" hora form-control" name="Operacao[hr_referencia]" maxlength="45" aria-invalid="false">
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                    </div>
                </div>
            </div>
        </fieldset>
        <?php ActiveForm::end(); ?>
        <p>
            <a href= "<?php echo Url::to(['recursooperacao/viewrelatoriorecursosoperacao','opid'=>$model->id])?>"><button class="btn btn-primary">Relatorio Recursos</button></a>
            <a href= "<?php echo Url::to(['recursooperacao/viewrecursosoperacao','opid'=>$model->id,'ogid'=>Usuario::findOne(Yii::$app->user->identity->usuario_id)->orgao_id])?>"><button class="btn btn-primary">Recursos do seu orgão</button></a>
            <a href= "<?php echo Url::to(['recursooperacao/create','id'=>$model->id])?>"><button class="btn btn-primary">Cadastrar Recurso</button></a>
            <a href= "<?php echo Url::to(['produtividadeoperacao/create','opid'=>$model->id])?>"><button class="btn btn-primary">Cadastrar produtividade</button></a>

            <?php if(Yii::$app->user->identity->id == $model->usuario_id):?>
            
                <?= Html::a('Alterar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Deletar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Tem certeza que deseja excluir esse registro?',
                        'method' => 'post',
                    ],
                ]) ?>
        </p>
    <?php endif;?>

    

</div>


<script src="../js/jquery-3.3.1.min.js" ></script>
<script src="../js/jquery.maskedinput.min.js" ></script>
<script>
    $(document).ready(function($){
        $('.data').mask("99/99/9999");
        $('.hora').mask("99:99");
    })
</script>