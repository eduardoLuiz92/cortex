<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OperacaoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="operacao-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nome') ?>

    <?= $form->field($model, 'municipio_desc') ?>

    <?= $form->field($model, 'municipio_id') ?>

    <?= $form->field($model, 'orgao_responsavel_id') ?>

    <?php // echo $form->field($model, 'dt_referencia') ?>

    <?php // echo $form->field($model, 'hr_referencia') ?>

    <?php // echo $form->field($model, 'usuario_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
