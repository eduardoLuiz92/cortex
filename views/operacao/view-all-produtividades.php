<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Municipio;
use app\models\Orgao;
use app\models\Usuario;
use app\models\Indicador;
use app\models\Localidade;


/* @var $this yii\web\View */
/* @var $searchModel app\models\OperacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Operações';
$this->params['breadcrumbs'][] = $this->title;
?>

        

<fieldset>
    <div class="col-md-12">
        <legend>Produtividades da Operação : <?php echo $model->nome; ?></legend>

        <?php if($produtividades):?>
            <?php foreach($produtividades as $prod): ?>
                <a target='_blank' href="<?php echo Url::to(['produtividadeoperacao/view','id'=>$prod->id])?>">
                    <div class="panel-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                
                                <p>
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <?php echo Orgao::findOne($prod->orgao_id)->descricao;?> | 
                                            <?php echo Usuario::findOne($prod->usuario_id)->username;?> |
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <b>
                                                <?php echo Indicador::findOne($prod->indicador_id)->descricao;?> | 
                                                <?php echo Localidade::findOne($prod->localidade_id)->nome;?>
                                            </b> 
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <b>Qtd: </b><?php echo $prod->qtd;?>
                                        </div>
                                        
                                        <div class="col-md-3">
                                            <?php echo $prod->data." - ".$prod->hora;?> 
                                        </div>
                                    </div>
                                    <br> 
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
            <?php endforeach;?>
        <?php endif;?>
    </div>    
</fieldset>
