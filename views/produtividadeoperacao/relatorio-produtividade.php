<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProdutividadeoperacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = ['label' => 'Operação', 'url' => ['operacao/view','id'=>$operacao->id]];
$this->title = 'Relatorio de produtividade dos orgãos';
$this->params['breadcrumbs'][] = $this->title;
$contadorLinhas = 1;
?>
<div class="col-md-12">
    <fieldset>
        <legend>Relatório de Produtividade</legend>
        <table class="table table-bordered">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Orgão</th>
                <th scope="col">Apreensão</th>
                <th scope="col">Prisão</th>
                <th scope="col">Notificação</th>
                <th scope="col">Flagrante</th>
                <th scope="col">Autuação</th>
                <th scope="col">Multa</th>
                <th scope="col">Rec. de CRLV</th>
                <th scope="col">Intervenção</th>
                <th scope="col">Condução</th>
                <th scope="col">Atendimento</th>
                <th scope="col">Abordagem</th>
                <th scope="col">interdição</th>
                <th scope="col">Alvará</th>
                <th scope="col">Outros</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($relatorio as $prod):?>
                    <tr>
                        <th scope="row"><?php echo $contadorLinhas?></th>
                        <td><?php echo $prod['Orgao'] ;?></td>
                        <td><?php echo $prod['Apreensao'] ;?></td>
                        <td><?php echo $prod['Prisao'] ;?></td>
                        <td><?php echo $prod['Notificacao'] ;?></td>
                        <td><?php echo $prod['Flagrante'] ;?></td>
                        <td><?php echo $prod['Autuacao'] ;?></td>
                        <td><?php echo $prod['Multa'] ;?></td>
                        <td><?php echo $prod['Recolhimento_de_CRLV'] ;?></td>
                        <td><?php echo $prod['Intervencao'] ;?></td>
                        <td><?php echo $prod['Conducao'] ;?></td>
                        <td><?php echo $prod['Atendimento'] ;?></td>
                        <td><?php echo $prod['Abordagem'] ;?></td>
                        <td><?php echo $prod['interdicao'] ;?></td>
                        <td><?php echo $prod['Alvara'] ;?></td>
                        <td><?php echo $prod['Outros'] ;?></td>
                    </tr>
                    <?php $contadorLinhas++;?>
                <?php endforeach; ?>
                <?php foreach($total_prods as $prod):?>
                    <tr>
                    <th scope="row"><?php echo $contadorLinhas?></th>
                        <td><?php echo "TOTAL";?></td>
                        <td><?php echo $prod['Apreensao'] ;?></td>
                        <td><?php echo $prod['Prisao'] ;?></td>
                        <td><?php echo $prod['Notificacao'] ;?></td>
                        <td><?php echo $prod['Flagrante'] ;?></td>
                        <td><?php echo $prod['Autuacao'] ;?></td>
                        <td><?php echo $prod['Multa'] ;?></td>
                        <td><?php echo $prod['Recolhimento_de_CRLV'] ;?></td>
                        <td><?php echo $prod['Intervencao'] ;?></td>
                        <td><?php echo $prod['Conducao'] ;?></td>
                        <td><?php echo $prod['Atendimento'] ;?></td>
                        <td><?php echo $prod['Abordagem'] ;?></td>
                        <td><?php echo $prod['interdicao'] ;?></td>
                        <td><?php echo $prod['Alvara'] ;?></td>
                        <td><?php echo $prod['Outros'] ;?></td>
                    </tr>
                    <?php $contadorLinhas++;?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </fieldset>
    <a href="<?php echo Url::to(['operacao/view','id'=>$operacao->id])?>"><div class="btn btn-info">Voltar</div></a>
</div>

<?php //echo "<pre>"; print_r($relatorio);?>