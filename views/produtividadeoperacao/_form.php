<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Produtividadeoperacao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="produtividadeoperacao-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'operacao_id')->textInput() ?>

    <?= $form->field($model, 'orgao_id')->textInput() ?>

    <?= $form->field($model, 'qtd')->textInput() ?>

    <?= $form->field($model, 'indicador_id')->textInput() ?>

    <?= $form->field($model, 'data')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hora')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usuario_id')->textInput() ?>

    <?= $form->field($model, 'localidade_id')->textInput() ?>

    <?= $form->field($model, 'descricao')->textArea(['rows'=>4,'disabled'=>true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
