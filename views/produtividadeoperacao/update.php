<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Produtividadeoperacao */

$this->title = 'Update Produtividadeoperacao: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Produtividadeoperacaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="produtividadeoperacao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-alterar', [
        'model' => $model,
    ]) ?>

</div>
