<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProdutividadeoperacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Produtividadeoperacaos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="produtividadeoperacao-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Produtividadeoperacao', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'operacao_id',
            'orgao_id',
            'qtd',
            'indicador_id',
            //'data',
            //'hora',
            //'usuario_id',
            //'localidade_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
