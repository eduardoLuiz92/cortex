<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Indicador;
use app\models\Localidade;


/* @var $this yii\web\View */
/* @var $model app\models\Produtividadeoperacao */
/* @var $form yii\widgets\ActiveForm */
$this->params['breadcrumbs'][] = ['label' => 'Operação', 'url' => ['operacao/view','id'=>$model->operacao_id]];
?>

<div class="produtividadeoperacao-form">
    <?php $form = ActiveForm::begin(); ?>
        <fieldset>
            <legend><h1>Produtividade</h1></legend>
            <div class="col-md-6">
                <?= $form->field($model, 'nome_operacao')->textInput(['disabled'=>true]) ?>

                <div class="form-group field-produtividadeoperacao-nome_usuario">
                    <label class="control-label" for="produtividadeoperacao-nome_usuario">Orgão da produtividade  </label>
                    <input type="text" id="produtividadeoperacao-nome_usuario" class="form-control" name="Produtividadeoperacao[nome_usuario]" value="<?php echo $model->nome_orgao; ?>" disabled="">

                    <div class="help-block"></div>
                </div>
                
                <div class="form-group field-operacao-latitude">
                    <label class="control-label" for="operacao-latitude">Indicador</label>
                    <?php echo Html::dropDownList('Produtividadeoperacao[indicador_id]', $model->indicador_id, 
                        ArrayHelper::map(Indicador::find()->all(), 'id', 'descricao'),
                        ['prompt'=>'Selecione ','name'=>'Produtividadeoperacao[indicador_id]','class'=>"form-control",'disabled'=>true]); 
                    ?>
                </div>

                <?= $form->field($model, 'qtd')->textInput(['disabled'=>true]) ?>
                
            </div>
            <div class="col-md-6">
                <div class="form-group field-operacao-latitude">
                    <label class="control-label" for="operacao-latitude">Local</label>
                    <?php echo Html::dropDownList('Produtividadeoperacao[localidade_id]', $model->localidade_id, 
                        ArrayHelper::map(Localidade::find()->all(), 'id', 'nome'),
                        ['prompt'=>'Selecione ','name'=>'Produtividadeoperacao[localidade_id]','class'=>"form-control",'disabled'=>true]); 
                    ?>
                </div>
                <div class="form-group field-produtividadeoperacao-nome_usuario">
                    <label class="control-label" for="produtividadeoperacao-nome_usuario">Usuário Resp. pela produtividade  </label>
                    <input type="text" id="produtividadeoperacao-nome_usuario" class="form-control" name="Produtividadeoperacao[nome_usuario]" value="<?php echo $model->nome_usuario;?>" disabled="">

                    <div class="help-block"></div>
                </div>
                <?= $form->field($model, 'data')->textInput(['maxlength' => true,'disabled'=>true]) ?>

                <?= $form->field($model, 'hora')->textInput(['maxlength' => true,'disabled'=>true]) ?>

                

                
            </div>

            <div class="col-md-12">
                <?= $form->field($model, 'descricao')->textArea(['rows'=>4,'disabled'=>true]) ?>
            </div>
            <a href="<?php echo Url::to(['operacao/view','id'=>$model->operacao_id])?>"><div class="btn btn-info">Voltar</div></a>
            <?php if(Yii::$app->user->identity->orgao_id == 23): ?>
                <?php if(Yii::$app->user->identity->user_lvl_di == 1): ?>
                    <a href="<?php echo Url::to(['update','id'=>$model->id])?>" ><div class="btn btn-warning">Alterar</div></a>
                <?php endif;?>
            <?php endif;?>
        </fieldset>

    <?php ActiveForm::end(); ?>

</div>
