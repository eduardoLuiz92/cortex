<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Produtividadeoperacao */

$this->title = 'Create Produtividadeoperacao';
$this->params['breadcrumbs'][] = ['label' => 'Produtividadeoperacaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="produtividadeoperacao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
