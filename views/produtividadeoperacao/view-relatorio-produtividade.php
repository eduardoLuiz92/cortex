<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProdutividadeoperacaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = ['label' => 'Operação', 'url' => ['operacao/view','id'=>$operacao->id]];
$this->title = 'Relatorio de produtividade dos orgãos';
$this->params['breadcrumbs'][] = $this->title;
$contadorLinhas = 1;
?>
<div class="col-md-12">
    <fieldset>
        <legend>Relatório de Produtividade</legend>
        <table class="table table-bordered">

            <?php foreach($relatorio as $chave_relatorio => $produtividade):?>
                <?php if($chave_relatorio == 0 ): ?>
                    <thead>
                        <tr>
                            <th scope="col"><?php echo "#"?></th>
                            <?php foreach($produtividade as $chave_produtividade => $valores_produtividade):?>    
                                <th scope="col"><?php echo $chave_produtividade?></th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                <?php endif; ?>
            <?php endforeach; ?>

            <tbody>
                <?php foreach($relatorio as $chave_relatorio => $produtividade):?>
                    <tr>
                        <th scope="row"><?php echo $contadorLinhas?></th>
                        <?php foreach($produtividade as $chave_produtividade => $valores_produtividade):?>    
                            <td><?php echo $valores_produtividade;?></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php $contadorLinhas++;?>
                <?php endforeach; ?>
            </tbody>

        
        </table>
    </fieldset>
    <a href="<?php echo Url::to(['operacao/view','id'=>$operacao->id])?>"><div class="btn btn-info">Voltar</div></a>
</div>