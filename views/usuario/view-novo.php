<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Orgao;
use app\models\Logincerebrum;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

        <fieldset>
            <Legend><h1>Visualizar Usuário</h1></legend>

            <div class="col-md-12">

                <div class="col-md-6">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true,'disabled'=>true]) ?>

                    <?= $form->field($model, 'nome_completo')->textInput(['maxlength' => true,'disabled'=>true]) ?>

                    <?= $form->field($model, 'cpf')->textInput(['maxlength' => true,'disabled'=>true]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'disabled'=>true]) ?>
                    
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'telefone')->textInput(['maxlength' => true,'disabled'=>true]) ?>

                    <?= $form->field($model, 'orgao_id')
                        ->dropDownList(
                            ArrayHelper::map(Orgao::find()->all(), 'id', 'sigla'),         // Flat array ('id'=>'label')
                            ['prompt'=>'Selecione ','name'=>'Usuario[orgao_id]', 'disabled'=>true]    // options
                    ); ?>

                    <div class="form-group field-operacao-latitude">
                        <label class="control-label" for="operacao-latitude">Tipo de Usuario</label>
                        <?= $form->field($login, 'user_lvl')->textInput(['maxlength' => true,'disabled'=>true])->label(false) ?>
                    </div>

                    <div class="form-group field-operacao-latitude">
                        <label class="control-label" for="operacao-latitude">Acesso</label>
                        <?php echo $form->field($login, 'status_acesso')->dropDownList(Logincerebrum::STATUS_ACESSO,['value'=>$login->status_acesso,'disabled'=>true])->label(false); ?>
                    </div>

                </div>

            </div>

            <?php echo Html::a('<div class="btn btn-info" onclick="loading()">Voltar</div>', Url::to(['usuario/index'])); ?>
            
            <?php 
                if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM){
                    echo Html::a('<div class="btn btn-warning" onclick="loading()">Alterar</div>', Url::to(['usuario/update','id'=>$model->id])); 
                }else if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM_ORGAO){
                    if($login->user_lvl != Logincerebrum::USUARIO_ADM){
                        echo Html::a('<div class="btn btn-warning" onclick="loading()">Alterar</div>', Url::to(['usuario/update','id'=>$model->id])); 
                    }
                }
            ?>

        </fieldset>

    <?php ActiveForm::end(); ?>

</div>
