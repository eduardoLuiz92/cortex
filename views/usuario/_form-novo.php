<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Orgao;
use app\models\Logincerebrum;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

        <fieldset>
            <Legend><h1>Criar Usuário</h1></legend>

            <div class="col-md-12">

                <div class="col-md-6">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'nome_completo')->textInput(['maxlength' => true]) ?>

                    <?php //echo $form->field($model, 'cpf')->textInput(['maxlength' => true]) ?>

                    <div class="form-group field-usuario-cpf required">
                        <label class="control-label" for="usuario-cpf">CPF</label>
                        <input type="text" id="usuario-cpf" class="form-control cpf" name="Usuario[cpf]" maxlength="45" aria-required="true" aria-invalid="true">
                    </div>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    
                </div>
                <div class="col-md-6">
                    <?php //echo $form->field($model, 'telefone')->textInput(['maxlength' => true]) ?>
                    <div class="form-group field-usuario-telefone">
                        <label class="control-label" for="usuario-telefone">Telefone</label>
                        <input type="text" id="usuario-telefone" class="form-control telefone" name="Usuario[telefone]" maxlength="45" aria-invalid="false">
                    </div>

                    <?= $form->field($model, 'orgao_id')
                        ->dropDownList(
                            ArrayHelper::map(Orgao::find()->all(), 'id', 'sigla'),         // Flat array ('id'=>'label')
                            ['prompt'=>'Selecione ','name'=>'Usuario[orgao_id]']    // options
                    ); ?>

                    <?php 
                        if(!Yii::$app->user->isGuest){
                            if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM){
                                echo  $form->field($login = new Logincerebrum(), 'user_lvl')->dropDownList(Logincerebrum::TIPOS_USUARIOS,['value'=>'COMUM'])->label('Tipo de Usuário'); 
                            }
                            if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM_ORGAO){
                                echo  $form->field($login = new Logincerebrum(), 'user_lvl')->dropDownList(Logincerebrum::TIPOS_USUARIOS_POR_ORGAO,['value'=>'COMUM'])->label('Tipo de Usuário'); 
                            }
                        }
                    ?>

                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                </div>

            </div>

        </fieldset>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="../js/jquery-3.3.1.min.js" ></script>
<script src="../js/jquery.maskedinput.min.js" ></script>
<script>

    let msg = "<?php echo $mensagem; ?>";

    function verificamsg(msg){
        if(msg){
            if(msg == "errocpf"){
                sweetAlert('CPF ja cadastrado!','Entre em contato para mais informações','warning');
            }
        }
    }
    $(document).ready(function($){
        $('.cpf').mask("999.999.999-99");
        $('.telefone').mask("(99)99999-9999");
        verificamsg(msg);
    })
</script>