<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\Orgao;
use app\models\Logincerebrum;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuários';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Cadastrar Usuário', ['create'], ['class' => 'btn btn-success']) ?> 
        <?php //echo Html::a('Conceder acesso', ['logincerebrum/create'], ['class' => 'btn btn-info']) ?> 
    </p>

    <?= 
    // GridView::widget([
    //     'dataProvider' => $dataProvider,
    //     'filterModel' => $searchModel,
    //     'columns' => [
    //         ['class' => 'yii\grid\SerialColumn'],

    //         // 'id',
    //         'username',
    //         // 'nome_completo',
    //         'cpf',
    //         'email:email',
    //         //'telefone',
    //         //'authKey',
    //         //'password',
    //         //'password_reset_token',
    //         //'user_level',
    //         'orgao_id',
    //         //'accessToken:ntext',
    //         //'user_lvl_di',

    //         ['class' => 'yii\grid\ActionColumn'],
    //     ],
    // ]); 
    
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['style' => 'width: 20px;', 'class' => 'text-center'],
            ],
            [
                'attribute' => 'username',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['style' => 'width: 30%; color:#337ab7;', 'class' => 'text-left'],
            ],
            [
                'attribute' => 'cpf',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
            ],    
            [
                'attribute' => 'orgao_id',
                'headerOptions' => ['class' => 'text-center'],
                'value'  => function ($data) {
                    return Orgao::findOne($data->orgao_id)->sigla;
                },
                'contentOptions' => ['style' => 'width: 20%; color:#337ab7;', 'class' => 'text-left'],
            ],            
            [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Ações', 
            'headerOptions' => ['width' => '80','style'=>'width: 20%; color:#337ab7;', 'class'=>'text-center'],
            'template' => '{view} {alterar} {acesso} {deletar}',
            'buttons' => [
                    'view' => function($url,$model,$key){
                            return Html::a('<div class="btn btn-info" onclick="loading()">Visualizar</div>', Url::to(['usuario/view','id'=>$key]));
                        
                    },
                    'alterar' => function($url,$model,$key){
                        if(Yii::$app->user->identity->orgao_id == 23){
                            return Html::a('<div class="btn btn-warning" onclick="loading()">alterar</div>', Url::to(['usuario/update','id'=>$key]));
                        }
                    },
                    'acesso' => function($url,$model,$key){

                        if(Yii::$app->user->identity->user_lvl != Logincerebrum::USUARIO_COMUM){
                            $acesso = Logincerebrum::verificaSeUsuarioTemAcesso($key);
                            if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM){
                                if($acesso == Logincerebrum::STATUS_INATIVO){
                                    return Html::a('<div class="btn btn-danger" onclick="loading()">Inativo</div>', Url::to(['logincerebrum/mudarstatus','id'=>$key]));
                                }else{
                                    return Html::a('<div class="btn btn-success" onclick="loading()">Ativo</div>', Url::to(['logincerebrum/mudarstatus','id'=>$key]));
                                }
                            }else if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM_ORGAO){
                                $login = Logincerebrum::find()->where(['usuario_id'=>$model->id])->one();
                                if($login->user_lvl != Logincerebrum::USUARIO_ADM){
                                    if($acesso == Logincerebrum::STATUS_INATIVO){
                                        return Html::a('<div class="btn btn-danger" onclick="loading()">Inativo</div>', Url::to(['logincerebrum/mudarstatus','id'=>$key]));
                                    }else{
                                        return Html::a('<div class="btn btn-success" onclick="loading()">Ativo</div>', Url::to(['logincerebrum/mudarstatus','id'=>$key]));
                                    }
                                }
                            }
                            
                            
                        }
                    },
                ],
            ],
        ],
    ]); 
    
    
    ?>
</div>
