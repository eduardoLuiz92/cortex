<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'CEREBRUM-AM';
?>

<style>
  .center{
    margin: 5% 0% 5% 40%;
  }
  .img-index {
    max-height: 270px;
    width: 30%;
    height: auto;
  }
  .jumbotron {
    text-align: center;
    background-color: transparent;
    padding-top: 0px; 
    padding-bottom: 0px; 
    margin-bottom: 0px; 
  }
  .bloco-texto{
    text-align: justify;
  }
</style>

<div class="site-index">
    <div class="row">
      <h1 class="jumbotron">CEREBRUM-AM</h1>
      <div id= "experience" class="jumbotron titulo">
        <?php echo Html::img("@web/images/logo-index-cicc.png",['class'=>'img-index']); ?>
          <!-- <h1>Business Intelligence</h1> -->
      </div>
      
    </div>
    
    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 bloco-texto ">
                <h2>Integração</h2>

                <p>Promove a integração dos orgãos competentes em relação a Segurança Pública, para que os serviços sejam feitos através de uma ferramenta única, simples e intuitiva.</p>

            </div>
            <div class="col-lg-4 bloco-texto ">
                <h2>Operações</h2>

                <p>Previamente e durante as operações realizadas, os orgãos envolvidos terão a possibilidade de cadastrar seus recursos empenhados e produtividades relacionadas a mesma, diretamente na ferramenta. Promovendo maior transparência e clareza, para que as decisões dos lideres situacionais se tornem ainda mais acertivas.</p>

              </div>
            <div class="col-lg-4 bloco-texto ">
                <h2>Produtividade</h2>

                <p>Com a possibilidade de cadastro de recursos e produtividade na ferramenta, será possivel gerar relatórios mais fiéis às informações das operações, de maneira mais rápida e sem emprenhar muitos recursos humanos.</p>

            </div>
        </div>

    </div>
</div>

<script src="../js/jquery-3.3.1.min.js" ></script>
<script>

function mostraMsg(lista_aux){
  let lista = [lista_aux];
  for (let i = 0; i < lista.length; i++) {
    let aux = lista[i].split('=');
    console.log(aux)
    if(aux[0] == 'mensagem'){
      if(aux[1] == 'sucesso'){
        sweetAlert('Sucesso','Solicitação de acesso enviada!','success')
      }else{
        sweetAlert(aux[1],'Algo inesperado aconteceu, tente novamente ou contate nosso suporte!','info')
      }
    }
  }
}

$(document).ready(function($){
  let msg = window.location.search.split('?')[1];
  console.log(msg);
  mostraMsg(msg);
}) 

</script>