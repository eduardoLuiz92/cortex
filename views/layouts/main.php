<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Orgao;
use app\models\Usuario;
use app\models\Logincerebrum;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
<?php
    NavBar::begin([
        // 'brandLabel' => Html::img('@web/images/logo-header.png', ['alt'=>"Cortex-Estadual", "class"=>"img-header",]),
                        // Html::label($this->title,null ,['alt'=>Yii::$app->name, "class"=>"container"]),
        'brandLabel' => 'CEREBRUM-AM',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $itensMenu = array(
        
        );

    if (Yii::$app->user->isGuest) {
        // $itensMenu[] = ['label' => 'Sobre', 'url' => ['/site/about']];
        // $itensMenu[] = ['label' => 'Contato', 'url' => ['/site/contact']];
        $itensMenu[] = ['label' => 'Login', 'url' => ['/site/login']];
        $itensMenu[] = ['label' => 'Solicitar Acesso', 'url' => ['usuario/solicitacaodeacesso']];
    }else{

        if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM_ORGAO){
            $itensMenu[] = ['label' => 'Usuários', 'url' => ['/usuario/index']];
            $itensMenu[] = ['label' => 'Operações', 'url' => ['/operacao/index']];
        }

        if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_ADM){
            $itensMenu[] = ['label' => 'Usuários', 'url' => ['/usuario/index']];
            $itensMenu[] = ['label' => 'Operações', 'url' => ['/operacao/index']];
            $itensMenu[] = ['label' => 'Indicadores', 'url' => ['/indicador/index']];
            $itensMenu[] = ['label' => 'Órgãos', 'url' => ['/orgao/index']];            
            $itensMenu[] = '<li><a target = "_blank" href="http://aviso.ssp.am.gov.br/eventos/">M. atividades</a></li>';

        }
        else if(Yii::$app->user->identity->user_lvl == Logincerebrum::USUARIO_COMUM){
            // $itensMenu[] = ['label' => 'Usuario', 'url' => ['/usuario/index']];
            $itensMenu[] = ['label' => 'Operacao', 'url' => ['/operacao/index']];
        }
        $itensMenu[] = '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    
                    'Logout (' . Yii::$app->user->identity->username .' - '.
                        Orgao::findOne(Usuario::findOne(Yii::$app->user->identity->usuario_id)->orgao_id)->sigla.
                        ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>';
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $itensMenu,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; SEAGI/CICC - AM <?= date('Y') ?></p>

        <p class="pull-right"><?= "Powered by SEAGI/CICC - AM" ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
