function loading(){
    swal({
        title: "Loading...",
        id:'load-modal',
        onOpen: () => {
            swal.showLoading()
            timerInterval = setInterval(() => {
            },100)
        },
    });
}

function encerraLoading(){
    swal.close();
}

function verificamensagem(mensagem){
    if(mensagem != ""){
        sweetAlert(mensagem,'','info');
    }
}
// sequencia exp: ("Sucesso", "Realizou com sucesso", "success")
function sweetAlert(tipo, titulo ,texto){
    swal({
        title:  titulo,
        type:   tipo,
        html:   texto,
        showCloseButton: false,
        showCancelButton: false,
        focusConfirm: true,
        confirmButtonText:
          '<i class="fa fa-thumbs-up"></i> Ok!',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        preConfirm: function () {
          
        }
    });
}